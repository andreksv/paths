package edu.ntnu.idatt2001.paths.data;

import edu.ntnu.idatt2001.paths.businessLogic.Link;
import edu.ntnu.idatt2001.paths.businessLogic.Passage;
import edu.ntnu.idatt2001.paths.businessLogic.Story;
import edu.ntnu.idatt2001.paths.businessLogic.actions.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FileHandling is a class for writing and reading
 * Story objects to and from a file.
 *
 * @author Andreas Kluge Svendsrud
 */
public class FileHandling {
    private static int currentLine = 0;
    /**
     * Method for writing (adding) a Story to a file.
     *
     * @param story the Story you want to write to a file.
     * @param filePath the path for where you want to store your story.
     * @throws IOException if an input or output exception occurred.
     */
    public static void writeStoryToFile(final Story story, final String filePath) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath + story.getTitle() + ".paths"))) {
            bw.write(story.toString());
        } catch (IOException ex) {
            throw new IOException("Error writing story to file: " + ex.getMessage());
        }
    }

    /**
     * Method for reading (getting) a Story from a .paths file.
     *
     * @param filePath the path to the file you want to read from.
     * @return story from the file with fileTitle.
     * @throws IOException if an input or output exception occurred.
     * @throws InvalidFileException if the file is not a paths file
     * @throws FileFormatException if the paths file has an invalid format
     */
    public static Story readStoryFromFile(String filePath) throws InvalidFileException, IOException, FileFormatException {
        String fileName = filePath.substring(filePath.lastIndexOf("\\")+1);
        if (!hasCorrectFileName(filePath)) {
            throw new InvalidFileException("This is not a paths file: " + fileName);
        }
        Story story = null;
        Passage passage = null;
        Link link = null;
        currentLine = 2;

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String storyTitle;
            if ((storyTitle = br.readLine()) == null) {
                throw new FileFormatException("The file is empty: " + fileName);
            }
            if (storyTitle.isEmpty()) {
                throw new FileFormatException("At line: 1 - Expected the Story Title, got an empty String");
            }
            String line;
            String nextLine = br.readLine();
            while ((line = nextLine) != null) {
                //Check if the line starts with one of
                //the possible special characters or if it is empty.
                //If this is not the case then the file has an invalid format.
                if (!(line.startsWith("@")) && !(line.startsWith("::")) && !(line.startsWith("[")) && !(line.startsWith("{")) && !(line.isBlank())) {
                    throw new FileFormatException("At line: " + currentLine + " - Invalid file format.\n" + line);
                }
                //Check if the line is a Passage.
                if (line.startsWith("::")) {
                    nextLine = br.readLine();
                    passage = readPassageFromFile(line, nextLine);
                    if (nextLine != null) {
                        line = nextLine;
                    }
                }
                //Check if the line is a Link.
                if (line.startsWith("[")) {
                    if (passage != null) {
                        if (link != null) {
                            passage.addLink(link);
                        }
                        link = readLinkFromFile(line);
                    } else {
                        throw new FileFormatException("At line: " + currentLine + " - There is not Passage defined for this Link: " + line);
                    }
                }
                //Check if the line is an Action.
                else if (line.startsWith("{")) {
                    if (link != null) {
                        link.addAction(readActionFromFile(line));
                    } else {
                        throw new FileFormatException("At line: " + currentLine + " - There is no Link defined for this Action: " + line);
                    }
                }

                nextLine = br.readLine();
                //Check if it is the end of a block (Passage) or end of file.
                if (((nextLine == null) || line.isBlank()) && (passage != null)) {
                    if (link != null) {
                        passage.addLink(link);
                        link = null;
                    }
                    //Check if this is the first Passage of the Story (openingPassage).
                    if (story == null) {
                        story = new Story(storyTitle, passage);
                    } else {
                        story.addPassage(passage);
                    }
                    passage = null;
                }
                currentLine++;
            }
        } catch(FileNotFoundException ex) {
            if (!hasCorrectFileName(fileName)) {
                throw new InvalidFileException("Invalid file name: " + fileName);
            } else {
                throw new FileNotFoundException("File not found: " + fileName);
            }
        } catch (IOException ex) {
            throw new IOException("Error reading story from file: " + ex.getMessage());
        } catch (FileFormatException ex) {
            throw new FileFormatException(ex.getMessage(), ex);
        }
        return story;
    }
    /**
     * Method for reading (getting) a Passage from a .paths file.
     *
     * @param line a String in the file where the Passage title is stored.
     * @param nextLine the line that comes after line in the file.
     * @return line converted into a Passage.
     */
    private static Passage readPassageFromFile(String line, String nextLine) {
        String passageTitle = line.replace("::", "");
        String passageContent = (nextLine != null && !nextLine.startsWith("[")) ? nextLine : "";
        return new Passage(passageTitle, passageContent);
    }

    /**
     * Method for reading (getting) a Link from a .paths file.
     *
     * @param line the line in the file where the Link is stored.
     * @return line converted into a Link.
     */
    private static Link readLinkFromFile(String line) {
        return new Link(line.substring(line.indexOf("[") + 1, line.indexOf("]")), line.substring(line.indexOf("(") + 1, line.indexOf(")")));
    }

    /**
     * Method for reading (getting) an action from a .paths file.
     *
     * @param line the line in the file where the Action is stored.
     * @return line converted into an Action.
     * @throws FileFormatException if the Action line has an invalid format.
     */
    private static Action readActionFromFile(String line) throws FileFormatException {
        String substring = line.substring(line.indexOf(":") + 1, line.indexOf("}"));
        try {
            if (line.startsWith("{G")) {
                return (new GoldAction(Integer.parseInt(substring)));
            } else if (line.startsWith("{H")) {
                return (new HealthAction(Integer.parseInt(substring)));
            } else if (line.startsWith("{S")) {
                return (new ScoreAction(Integer.parseInt(substring)));
            } else if (line.startsWith("{I")) {
                return (new InventoryAction(substring));
            } else {
                throw new FileFormatException("At line: " + currentLine + " - Invalid file format.\n" + line);
            }
        } catch(NumberFormatException ex){
            throw new FileFormatException("At line: " + currentLine + " - Invalid file format.\n" + line);
        }
    }

    /**
     * Method for checking if the fileName
     * is for a .paths file.
     *
     * @param fileName the file name you want to check.
     * @return true or false depending on if fileName is for a .paths file.
     */
    private static boolean hasCorrectFileName(String fileName){
        return fileName.contains(".paths");
    }
}
