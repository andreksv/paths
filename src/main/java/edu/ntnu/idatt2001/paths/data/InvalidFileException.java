package edu.ntnu.idatt2001.paths.data;

public class InvalidFileException extends Exception {
    public InvalidFileException(String errorMessage){
        super(errorMessage);
    }
}
