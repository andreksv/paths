package edu.ntnu.idatt2001.paths.data;

/**
 * Class that is an Exception for when
 * a File has an incorrect format.
 *
 * @author Andreas Kluge Svendsrud
 */
public class FileFormatException extends Exception {
    /**
     * Method for throwing an exception, with both the errorMessage
     * and the Throwable.
     *
     * @param errorMessage the error message from the exception.
     * @param err the Throwable from the exception.
     */
    public FileFormatException(String errorMessage, Throwable err){
        super(errorMessage, err);
    }

    /**
     * Method for throwing an exception, with just errorMessage.
     *
     * @param errorMessage the error message from the exception.
     */
    public FileFormatException(String errorMessage){
        super(errorMessage);
    }
}
