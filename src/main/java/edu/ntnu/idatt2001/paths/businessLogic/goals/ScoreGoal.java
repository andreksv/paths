package edu.ntnu.idatt2001.paths.businessLogic.goals;

import edu.ntnu.idatt2001.paths.businessLogic.Player;

/**
 * The class represents an expected minimum score.
 *
 * @author Andreas Kluge Svendsrud
 */
public class ScoreGoal implements Goal{
    private final int minimumPoints;

    /**
     * Class constructor.
     *
     * @param minimumPoints the minimum score needed to fulfill this goal.
     * @throws IllegalArgumentException if minimumPoints is less than zero.
     */
    public ScoreGoal(int minimumPoints) throws IllegalArgumentException {
        if (minimumPoints < 0) {
            throw new IllegalArgumentException("minimumPoints cannot be less than zero");
        }
        this.minimumPoints = minimumPoints;
    }

    /**
     * Checks if the goal has been achieved.
     *
     * @param player the Player you want to check.
     * @return true or false depending on  if the Player as a score equal to or higher than the minimum score.
     */
    @Override
    public boolean isFulfilled(Player player){
        return player.getScore() > this.minimumPoints;
    }

    @Override
    public String toString() {
        return "ScoreGoal\n" + minimumPoints;
    }
}
