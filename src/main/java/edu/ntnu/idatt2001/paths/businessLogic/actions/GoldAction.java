package edu.ntnu.idatt2001.paths.businessLogic.actions;

import edu.ntnu.idatt2001.paths.businessLogic.Player;

/**
 * A class that changes the gold amount of a Player.
 *
 * @author Anders Høvik
 */
public class GoldAction implements Action {
    private final int gold;

    /**
     * Class constructor.
     *
     * @param gold the change of gold to a Player.
     */
    public GoldAction(int gold){
        this.gold = gold;
    }

    /**
     * Executes the change in gold-value to a Player.
     *
     * @param player the Player that will change its gold amount.
     */
    @Override
    public void execute(Player player) {
        player.addGold(this.gold);

    }

    @Override
    public String toString() {
        return "{GoldAction:" + gold +
                '}';
    }
}
