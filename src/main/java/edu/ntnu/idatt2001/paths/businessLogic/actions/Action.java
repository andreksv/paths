package edu.ntnu.idatt2001.paths.businessLogic.actions;

import edu.ntnu.idatt2001.paths.businessLogic.Player;

/**
 * An interface that represents a change to a players attributes.
 *
 * @author Anders Høvik
 */
public interface Action {
    /**
     * Executes the change of attributes to a player.
     *
     * @param player the player you want to affect.
     */
     void execute(Player player);
}
