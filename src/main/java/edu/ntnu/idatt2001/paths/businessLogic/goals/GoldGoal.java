package edu.ntnu.idatt2001.paths.businessLogic.goals;

import edu.ntnu.idatt2001.paths.businessLogic.Player;

/**
 * The class represents an expected minimum gold amount.
 *
 * @author Andreas Kluge Svendsrud
 */
public class GoldGoal implements Goal {
    private final int minimumGold;

    /**
     * Class constructor.
     *
     * @param minimumGold the minimum gold amount needed to fulfill this goal.
     * @throws IllegalArgumentException if minimumGold is less than zero.
     */
    public GoldGoal(int minimumGold) throws IllegalArgumentException {
        if (minimumGold < 0 ) {
            throw new IllegalArgumentException("minimumGold cannot be less than zero");
        }
        this.minimumGold = minimumGold;
    }


    /**
     * Checks if the goal has been achieved.
     *
     * @param player the Player you want to check.
     * @return true or False if the Player as a gold amount equal to or higher than the minimum gold amount.
     */
    @Override
    public boolean isFulfilled(Player player) {
        return player.getGold() > this.minimumGold;
    }

    @Override
    public String toString() {
        return "GoldGoal\n" + minimumGold+"g";
    }
}
