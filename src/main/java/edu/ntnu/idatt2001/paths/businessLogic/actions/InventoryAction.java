package edu.ntnu.idatt2001.paths.businessLogic.actions;
import edu.ntnu.idatt2001.paths.businessLogic.Player;

/**
 * Class that adds an item to the inventory of a Player.
 *
 * @author Anders Høvik
 */
public class InventoryAction implements Action {
    private final String item;

    /**
     * Class constructor.
     *
     * @throws NullPointerException if item is null
     * @param item the item that will be added.
     */
    public InventoryAction(String item) {
        if (item == null) {
            throw new NullPointerException("Item cannot be null");
        }
        this.item = item;
    }

    /**
     * Execute the action of adding an item to a player.
     *
     * @param player the Player that will have an item added.
     */
    @Override
    public void execute(Player player) throws IllegalArgumentException, NullPointerException{
        player.addToInventory(this.item);
    }

    @Override
    public String toString() {
        return "{InventoryAction:" + item +
                '}';
    }
}
