package edu.ntnu.idatt2001.paths.businessLogic;

import edu.ntnu.idatt2001.paths.businessLogic.actions.Action;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A Link makes it possible to go from one Passage to another.
 * Link´s bind together the different parts of a Story.
 *
 * @author Andreas Kluge Svendsrud
 */
public class Link {
    /**
     * text is a descriptive text which indicate a choice
     * or an action for a story.
     */
    private final String text;

    /**
     * reference is a string which identifies a passage.
     * In practice, it´s title.
     */
    private final String reference;

    /**
     * actions is a list of special objects that make it possible to affect
     * the characteristics of a Player.
     */
    private final List<Action> actions;

    /**
     * Class constructor.
     *
     * @param text a descriptive text which indicate a choice or an action for a story.
     * @param reference a string which identifies a passage. In practice, it´s title.
     * @throws NullPointerException if text or reference is null.
     */
    public Link(final String text, final String reference) throws NullPointerException {
        if (text == null) {
            throw new NullPointerException("text cannot be null");
        }
        if (reference == null) {
            throw new NullPointerException("reference cannot be null");
        }
        this.text = text;
        this.reference = reference;
        this.actions = new ArrayList<>();
    }

    /**
     * Returns the text of the Link.
     *
     * @return the Link´s text.
     */
    public String getText() {
        return text;
    }

    /**
     * Returns the reference of the Link.
     *
     * @return the Link´s reference.
     */
    public String getReference() {
        return reference;
    }

    /**
     * Add an Action object to actions.
     *
     * @param action the Action object you want to add.
     * @throws IllegalArgumentException if the action already is in the List.
     */
    public void addAction(final Action action) throws NullPointerException, IllegalArgumentException {
        if (action == null) {
            throw new NullPointerException("action cannot be null");
        }
        if (actions.contains(action)) {
                throw new IllegalArgumentException("This action is already in the list");
            }
        actions.add(action);
    }

    /**
     * Returns a list of the Action-objects in actions.
     *
     * @return actions.
     */
    public List<Action> getActions() {
        return actions;
    }

    @Override
    public String toString() {
        StringBuilder actionsString = new StringBuilder();
        for (Action action : actions) {
            actionsString.append("\n").append(action.toString());
        }
        return "[" + this.getText() + "]" + "(" + this.getReference() + ")" + actionsString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Link link = (Link) o;
        return Objects.equals(reference, link.reference);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reference);
    }
}
