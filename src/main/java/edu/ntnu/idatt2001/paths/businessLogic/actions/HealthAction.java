package edu.ntnu.idatt2001.paths.businessLogic.actions;
import edu.ntnu.idatt2001.paths.businessLogic.Player;

/**
 * Class that changes the health value of a Player.
 *
 * @author Anders Høvik
 */
public class HealthAction implements Action {
    private final int health;

    /**
     * Class constructor.
     *
     * @param health the change of health to a Player.
     */
    public HealthAction(int health){
    this.health = health;
    }

    /**
     * Executes the change in health value to a Player.
     *
     * @param player the Player that will change its health value.
     */
    @Override
    public void execute(Player player) {
       player.addHealth(this.health);
    }

    @Override
    public String toString() {
        return "{HealthAction:" + health +
                '}';
    }
}
