package edu.ntnu.idatt2001.paths.businessLogic;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A class that represents a player of the Game.
 *
 * @author Anders Høvik
 */
public class Player {
    /**
     * The name of the player.
     */
    private final String name;
    /**
     * The health of the player.
     */
    private int health;
    /**
     * The score amount of the player.
     */
    private int score;
    /**
     * The gold amount of the player.
     */
    private int gold;
    /**
     * List of items represented as Strings.
     */
    private final List<String> inventory;

    /**
     * Class constructor using Builder design pattern.
     *
     * @param builder uses class Builder to create a Player.
     */

    private Player(Builder builder) {
        name = builder.name;
        health = builder.health;
        score = builder.score;
        gold = builder.gold;
        inventory = builder.inventory;
    }

    /**
     * Builder design pattern for
     * Player object.
     */
    public static class Builder {
        private String name = "Hero";
        private int health = 100;
        private int score = 0;
        private int gold = 0;
        private List<String> inventory = new ArrayList<>();

        /**
         * Method for setting the name
         * of the Player, in the constructor.
         *
         * @param name the name you want to set for the Player.
         * @return name
         * @throws NullPointerException if name is null.
         * @throws IllegalArgumentException if name is a blank String.
         */
        public Builder name(String name) throws NullPointerException, IllegalArgumentException {
            if (name == null) {
                throw new NullPointerException("Name cannot be null");
            }
            if (name.isBlank()) {
                throw new IllegalArgumentException("Name cannot be a blank String");
            }
            this.name = name;
            return this;
        }

        /**
         * Method for setting the health
         * of the Player, in the constructor.
         *
         * @param health the health value you want to set for the Player.
         * @return health
         * @throws IllegalArgumentException if the provided health value is less than zero.
         */
        public Builder health(int health) throws IllegalArgumentException {
            if (health < 0) {
                throw new IllegalArgumentException("Health cannot be less than zero");
            }
            this.health = health;
            return this;
        }

        /**
         * Method for setting the score amount
         * of the Player, in the constructor.
         *
         * @param score the score value you want to set for the Player.
         * @return score
         * @throws IllegalArgumentException if the provided score value is less than zero.
         */
        public Builder score(int score) throws IllegalArgumentException {
            if (score < 0) {
                throw new IllegalArgumentException("Score cannot be less than zero");
            }
            this.score = score;
            return this;
        }

        /**
         * Method for setting the gold amount
         * of the Player, in the constructor.
         *
         * @param gold the gold value you want to set for the Player.
         * @return gold
         * @throws IllegalArgumentException if the provided gold value is less than zero
         */
        public Builder gold(int gold) throws IllegalArgumentException {
            if (gold < 0) {
                throw new IllegalArgumentException("Gold cannot be less than zero");
            }
            this.gold = gold;
            return this;
        }

        /**
         * Method for setting the inventory
         * of the Player, in the constructor.
         *
         * @param inventory a List of Strings you want to set as the inventory of the Player.
         * @return inventory
         * @throws NullPointerException if inventory is null.
         */
        public Builder inventory(List<String> inventory) throws NullPointerException {
            if (inventory == null) {
                throw new NullPointerException("Inventory cannot be null");
            }
            this.inventory = inventory;
            return this;
        }

        public Player build() {
            return new Player(this);
        }
    }

    /**
     * Get the name of the Player.
     *
     * @return the Player´s name.
     */
    public String getName() {
        return name;
    }

    /**
     * Add an amount to the health value of the Player.
     * If the health would have been less than zero,
     * then it is set to zero.
     *
     * @param health amount you want to add to the health of the Player.
     */
    public void addHealth(int health) {
        if (this.health + health < 0) {
           this.health = 0;
        } else {
            this.health += health;
        }
    }

    /**
     * Get the health value of the Player.
     *
     * @return the player´s health-value.
     */
    public int getHealth() {
        return health;
    }

    /**
     * Add an amount to the score of the Player.
     *
     * @param points the amount you want to add to the score of the Player.
     */
    public void addScore(int points) {
        if (points + this.score <= 0) {
            this.score = 0;
        } else {
            this.score += points;
        }
    }

    /**
     * Get the score of the Player.
     *
     * @return the Player´s score.
     */
    public int getScore() {
        return score;
    }

    /**
     * Add an amount to the gold amount of the Player.
     * If the gold would have been less than zero,
     * then it is set to zero.
     *
     * @param gold the amount you want to add to the gold amount of the Player.
     */
    public void addGold(int gold) {
        if (this.gold + gold <= 0) {
            this.gold = 0;
        } else {
            this.gold += gold;
        }
    }

    /**
     * Get the gold amount of the player.
     *
     * @return the Player´s gold amount.
     */
    public int getGold() {
        return gold;
    }

    /**
     * Add an item to the Player´s inventory.
     *
     * @param item the item you want to add to the Player´s inventory.
     * @throws NullPointerException if the item is null.
     * @throws IllegalArgumentException if item is an empty String.
     */
    public void addToInventory(String item) throws NullPointerException, IllegalArgumentException {
        if (item == null) {
            throw new NullPointerException("The item is null");
        } else if (item.equals("")) {
            throw new IllegalArgumentException("The String can't be empty");
        } else {
             this.inventory.add(item);
        }

    }

    /**
     * Get the inventory-list of the Player.
     *
     * @return the Player´s inventory-list.
     */
    public List<String> getInventory() {
        return this.inventory;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", health=" + health +
                ", score=" + score +
                ", gold=" + gold +
                ", inventory=" + inventory +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return health == player.health && score == player.score && gold == player.gold && Objects.equals(name, player.name) && Objects.equals(inventory, player.inventory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, health, score, gold, inventory);
    }
}
