package edu.ntnu.idatt2001.paths.businessLogic.goals;
import edu.ntnu.idatt2001.paths.businessLogic.Player;

/**
 * A Goal represents an objective
 * or a desired result related to the player's condition.
 *
 * @author Andreas Kluge Svendsrud
 */
public interface Goal {
    /**
     * A Goal represents a target value or a
     * desired result related to a Player's state.
     *
     * @param player the Player you want to check.
     * @return true or false depending on if the goal is fulfilled.
     */
    boolean isFulfilled(Player player);
}
