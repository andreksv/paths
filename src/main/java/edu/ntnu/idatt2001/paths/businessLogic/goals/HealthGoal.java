package edu.ntnu.idatt2001.paths.businessLogic.goals;

import edu.ntnu.idatt2001.paths.businessLogic.Player;

/**
 * The class represents an expected minimum health value.
 *
 * @author Andreas Kluge Svendsrud
 */
public class HealthGoal implements Goal {
    private final int minimumHealth;

    /**
     * Class constructor.
     *
     * @param minimumHealth the minimum health value needed to fulfill this goal.
     * @throws IllegalArgumentException if minimumHealth is less than zero.
     */
    public HealthGoal(int minimumHealth) throws IllegalArgumentException {
        if (minimumHealth < 0) {
            throw new IllegalArgumentException("minimumHealth cannot be less than zero");
        }
        this.minimumHealth = minimumHealth;
    }

    /**
     * Checks if the goal has been achieved.
     * @param player the Player you want to check.
     * @return true or false if the Player has a health value equal to or higher than the minimum health.
     */
    @Override
    public boolean isFulfilled(Player player) {
        return player.getHealth() > this.minimumHealth;

    }

    @Override
    public String toString() {
        return "HealthGoal\n" + minimumHealth + "hp";
    }
}
