package edu.ntnu.idatt2001.paths.businessLogic.actions;
import edu.ntnu.idatt2001.paths.businessLogic.Player;

/**
 * A class that changes the score value of a Player.
 *
 * @author Anders Høvik
 */
public class ScoreAction implements Action {
    private final int points;

    /**
     * Class constructor.
     *
     * @param points the change in score to a Player.
     */
    public ScoreAction(int points){
        this.points = points;
    }

    /**
     * Executes the change in score.
     *
     * @param player the player that will change its score amount.
     */
    @Override
    public void execute(Player player) {
        player.addScore(this.points);
    }

    @Override
    public String toString() {
        return "{ScoreAction:" + points +
                '}';
    }
}
