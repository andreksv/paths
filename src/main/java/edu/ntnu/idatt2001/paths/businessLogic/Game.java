package edu.ntnu.idatt2001.paths.businessLogic;

import edu.ntnu.idatt2001.paths.businessLogic.goals.Goal;

import java.util.ArrayList;
import java.util.List;

/**
 * Game is the facade for a Paths-game.
 * The class connects a Player up to a Story with a List of Goal´s,
 * and it contains method for starting and maneuvering in a Paths-game.
 *
 * @author Andreas Kluge Svendsrud
 */
public class Game {
    /**
     * This is the player of the game
     * and has different attributes that can be affect in the story.
     */
    private Player player;
    /**
     * A story is an interactive, non-linear narrative,
     * containing a collection of passages.
     */
    private final Story story;
    /**
     * A list of special objects that determine
     * the desired results of a game.
     */
    private final List<Goal> goals;

    /**
     * Class constructor.
     *
     * @param player this is the player of the game and has different attributes that can be affect in the story.
     * @param story a story is an interactive, non-linear narrative, containing a collection of passages.
     * @param goals a list of special objects that determine the desired results of a game.
     * @throws NullPointerException if player, story or goals is null.
     */
    public Game(final Player player, final Story story, final List<Goal> goals) {
        if (player == null) {
            throw new NullPointerException("player cannot be null");
        }
        if (story == null) {
            throw new NullPointerException("story cannot be null");
        }
        if( goals == null) {
            throw new NullPointerException("goals cannot be null");
        }
        this.player = player;
        this.story = story;
        this.goals = goals;
    }

    /**
     * Return the player of the Game.
     *
     * @return the Game´s player.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Set player to a given Player-object.
     *
     * @param player the Player-object you want to use.
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * Return the story of the Game.
     *
     * @return the Game´s story.
     */
    public Story getStory() {
        return story;
    }

    /**
     * Return the goals of the Game.
     *
     * @return the Game´s goals.
     */
    public List<Goal> getGoals() {
        return goals;
    }

    /**
     * Method for adding a Goal to gaols.
     *
     * @param goal the Goal you want to add.
     */
    public void addGoal(Goal goal) {
        goals.add(goal);
    }

    /**
     * Method for getting a List
     * of every Goal that is fulfilled.
     *
     * @return List of fulfilled Goal´s.
     */
    public List<Goal> getFulfilledGoals() {
        List<Goal> fulfilledGoals = new ArrayList<>();
        for (Goal goal : goals) {
            if (goal.isFulfilled(player)) {
                fulfilledGoals.add(goal);
            }
        }
        return fulfilledGoals;
    }
    /**
     * Return the first passage of the Story.
     *
     * @return the Story´s first passage.
     */
    public Passage begin(){
        return story.getOpeningPassage();
    }

    /**
     * Return the Passage that matches the given Link.
     *
     * @param link the Link to the Passage you want to find.
     * @return the Passage with the given Link.
     * @throws NullPointerException if there is no Passage with the given link.
     */
    public Passage go(Link link) throws NullPointerException {
        Passage passage = story.getPassage(link);
        executeActions(link);
        return passage;
    }

    /**
     * Method that executes any Action´s connected to a Link
     * on player.

     * @param link the Link you want to execute the Action´s from.
     */
    public void executeActions(Link link) {
        link.getActions().forEach(action -> action.execute(getPlayer()));
    }
}
