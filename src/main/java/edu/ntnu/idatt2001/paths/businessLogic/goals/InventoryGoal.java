package edu.ntnu.idatt2001.paths.businessLogic.goals;

import edu.ntnu.idatt2001.paths.businessLogic.Player;
import java.util.List;

/**
 * The class represents an expected inventory of items.
 *
 * @author Andreas Kluge Svendsrud
 */
public class InventoryGoal implements Goal {
    private final List<String> mandatoryItems;

    /**
     * Class constructor.
     *
     * @throws NullPointerException if mandatoryItems is null.
     * @param mandatoryItems a list of items needed to fulfill this goal.
     */
    public InventoryGoal(List<String> mandatoryItems) {
        if (mandatoryItems == null) {
            throw new NullPointerException("mandatoryItems cannot be null");
        }
        this.mandatoryItems = mandatoryItems;
    }

    /**
     * Checks if the goal has been achieved.
     *
     * @param player the Player you want to check.
     * @return true or false depending on if the Player has all the mandatory items in their inventory.
     */
    @Override
    public boolean isFulfilled(Player player) {
        for(String item : this.mandatoryItems){
            if(!player.getInventory().contains(item)){
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "InventoryGoal\n" + mandatoryItems;
    }
}
