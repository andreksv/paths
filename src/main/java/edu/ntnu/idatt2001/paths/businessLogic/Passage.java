package edu.ntnu.idatt2001.paths.businessLogic;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A class that represents a chapter of a Story.
 *
 * @author Anders Høvik
 */
public class Passage {
    /**
     * title is an overall description
     * that also serves as an identifier.
     */
    private final String title;
    /**
     * textual content that typically
     * represents a paragraph or part of a dialogue.
     */
    private final String content;
    /**
     * List of Links that connect this passage
     * to other passages.
     */
    private final List<Link> links;

    /**
     * Class constructor.
     *
     * @param title an overall description that also serves as an identifier.
     * @param content textual content that typically represents a paragraph or part of a dialogue.
     * @throws NullPointerException if title or content is null.
     */
   public Passage(final String title, final String content) throws NullPointerException {
       if (title == null) {
           throw new NullPointerException("title cannot be null");
       }
       if (content == null) {
           throw new NullPointerException("content cannot be null");
       }
        this.title = title;
        this.content = content;
        this.links = new ArrayList<>();
    }

    /**
     * Get the title of a Passage.
     *
     * @return the Passage´s title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Get the content of the Passage.
     *
     * @return the Passage´s content.
     */
    public String getContent() {
        return content;
    }

    /**
     * Add a Link to the Passage.
     *
     * @param link the Link that you want to add.
     * @return true or false depending on if the Link was added.
     */
    public boolean addLink(Link link) {
        if (links.contains(link)) {
            return false;
        } else {
            links.add(link);
            return true;
        }
    }

    /**
     * Get a list of links that can be
     * used to connect to other passages.
     *
     * @return links.
     */
    public List<Link> getLinks() {
        return links;
    }

    /**
     * Checks if the Passage has links.
     *
     * @return true or false whether the passage has links.
     */
    public boolean hasLinks(){
        return !getLinks().isEmpty();
    }

    @Override
    public String toString() {
        StringBuilder linkString = new StringBuilder();
        for (Link link : links) {
            linkString.append("\n").append(link.toString());
        }
        if (content.isEmpty()) {
            return "::" + title + linkString;
        }
        else {
            return "::" + title + '\n' + content + linkString;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) return false;
        Passage passage = (Passage) o;
        return Objects.equals(title, passage.title) && Objects.equals(content, passage.content) && Objects.equals(links, passage.links);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, content, links);
    }
}
