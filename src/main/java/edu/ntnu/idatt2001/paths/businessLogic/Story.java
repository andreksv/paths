package edu.ntnu.idatt2001.paths.businessLogic;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A story is an interactive, non-linear narrative
 * containing a collection of Passages.
 *
 * @author Andreas Kluge Svendsrud
 */
public class Story {
    /**
     * The description of the story
     */
    private String description;
    /**
     * The title of the story.
     */
    private final String title;
    /**
     * A map containing the Passages of the Story.
     * The key to each passage is a link.
     */
    private final Map<Link, Passage> passages;
    /**
     * The first Passage of the story.
     */
    private final Passage openingPassage;

    /**
     * Class constructor without description.
     *
     * @param title the title of the story.
     * @param openingPassage the first Passage of the story.
     * @throws NullPointerException if title or openingPassage is null.
     */
    public Story(String title, Passage openingPassage) throws NullPointerException {
        if (title == null) {
            throw new NullPointerException("title cannot be null");
        }
        if (openingPassage == null) {
            throw new NullPointerException("openingPassage cannot be null");
        }
        this.title = title;
        this.openingPassage = openingPassage;
        this.passages = new HashMap<>();
        //Adds the openingPassage to passages
        addPassage(openingPassage);
    }

    /**
     * Returns the title of the Story.
     *
     * @return the Story´s title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns the openingPassage of the Story.
     *
     * @return the Story´s first Passage
     */
    public Passage getOpeningPassage() {
        return openingPassage;
    }

    /**
     * Add a Passage object to passages.
     * Method creates a Link object that has
     * text and reference equal to the title of the Passage.
     * This will function as the Key in passages.
     *
     * @param passage the Passage object you want to add.
     * @throws IllegalArgumentException if the Passage already is in passages.
     */
    public void addPassage(Passage passage) throws IllegalArgumentException {
        Link link = new Link(passage.getTitle(),passage.getTitle());
        if (this.passages.containsKey(link)) {
            throw new IllegalArgumentException("This Passage is already in passages");
        }
        passages.put(link, passage);
    }

    /**
     * Returns a specific Passage from passages.
     *
     * @param link the Link (key) to the Passage you want to find.
     * @return the passage with the given Link in passages.
     * @throws NullPointerException if there is no Passage with the given link.
     */
    public Passage getPassage(Link link) throws NullPointerException {
        return this.passages.get(link);
    }

    /**
     * Returns a Collection of the Passage´s in passages.
     *
     * @return the Passage´s in passages.
     */
    public Collection<Passage> getPassages() {
        return this.passages.values();
    }

    /**
     * Returns a List that contains every
     * Link from every Passage in passages.
     *
     * @return a List of every Link in passages.
     */
    public List<Link> getLinks() {
        return getPassages().stream()
                .map(Passage::getLinks).toList()
                .stream().flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    /**
     * Finds and returns a list of "dead Links". A Link is dead if
     * it refers to a passage not found in passages.
     *
     * @return a List of dead Links.
     */
    public List<Link> getBrokenLinks() {
        return getLinks().stream()
                .filter(link -> passages.get(link)==null)
                .collect(Collectors.toList());
    }

    /**
     * Finds and returns a list of "dead Passages". A Passage is dead if
     * there is no Link that refers to it in passages.
     *
     * @return a List of dead Passages.
     */
    public List<Passage> getBrokenPassages() {
        return getPassages().stream()
                .filter(passage -> getLinks().stream().noneMatch(link -> getPassage(link) == passage))
                .collect(Collectors.toList());
    }

    /**
     * Remove a Passage from passages. Only possible
     * to remove a passage if there are no Links
     * that refers to it in passages (it´s a "dead Passage").
     *
     * @param link a Link to the Passage you want to remove.
     * @throws IllegalArgumentException if there are other passages linking to the passage you want to remove.
     * @throws NullPointerException if there is no Passage with the given link.
     */
    public void removePassage(Link link) throws IllegalArgumentException, NullPointerException {
        Passage passageWantRemove = getPassage(link);
        if (!getBrokenPassages().contains(passageWantRemove)) {
            throw new IllegalArgumentException("Not possible to remove passage since there are other passages linking to it");
        } else {
            passages.remove(link);
        }
    }

    @Override
    public String toString() {
        StringBuilder passagesString = new StringBuilder();
        passagesString.append("\n\n").append(openingPassage.toString());
        for(Passage passage : getPassages()){
            if(!(passage==openingPassage)){
                passagesString.append("\n\n").append(passage.toString());
            }
        }
        return title+passagesString;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Story story = (Story) o;
        return Objects.equals(title, story.title) && Objects.equals(passages, story.passages) && Objects.equals(openingPassage, story.openingPassage);
    }
    @Override
    public int hashCode() {
        return Objects.hash(title, passages, openingPassage);
    }
}
