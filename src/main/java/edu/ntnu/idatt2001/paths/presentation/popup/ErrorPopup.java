package edu.ntnu.idatt2001.paths.presentation.popup;

import javafx.scene.control.Alert;

public class ErrorPopup {
    public static void showErrorPopup(String message, String exceptionName) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(exceptionName);
        alert.setHeaderText(exceptionName);
        alert.setContentText(message);
        alert.showAndWait();
    }
}