package edu.ntnu.idatt2001.paths.presentation.popup;

import java.util.HashMap;
import java.util.Map;

/**
 * Class for storing and accessing text
 * for specific scenes in GUI.
 *
 * @author Andreas Kluge Svendsrud
 */
public class Help {
    /**
     * Map where the key is a String
     * that represents the title of the Scene, and a value
     * that is helpful text about the Scene.
     */
    private static final Map<String, String> helpMap;

    /**
     * Here is where you add an element
     * to the Map for a new scene.
     */
    static {
        helpMap = new HashMap<>();
        helpMap.put("mainMenu","Welcome to the game of Paths. This is a game engine for choice-based and interactive story games.\n\n Create the game you want to play by picking from multiple possible Stories, designing your own characters and picking the goals of the game!\n\n If this is your first time playing click on the \"Pick a Story!\" to get started and choosing which story you will play.");
        helpMap.put("createStory","Here you are able to create your own Story! A story is an interactive, non-linear narrative containing a title and collection of Passages.");
        helpMap.put("createPassage", "Here you are able to create your own Passage! A Passage represents a chapter of a Story, it has a title that is an overall description, textual content that typically represents a paragraph or part of a dialog and a List of Links that connect this passage to other passages.");
        helpMap.put("createLink", "Here you are able to create your own Link! A Link makes it possible to go from one Passage to another, it binds together the different parts of a Story!\n\nA Link has a descriptive text which indicate a choice or an action for a story. A reference which is a string that identifies passage. In practice, it´s title. And a list of Actions special objects that make it possible to affect the characteristics of a Player");
        helpMap.put("createAction", "Here you are able to create your own Action! Actions are use to affect a Players attribute in different ways. GoldAction changes the gold amount of a Player, HealthAction changes the health amount of a Player, ScoreAction changes the score amount of a Player and InventoryAction gives an item to a Player");
        helpMap.put("goalSelection", "Here you are able to manage what Goals you want to include in your Story! A goal represents a target value or a desired outcome associated with the player's state.\n\nWhile actions modify the player's state along the way, goals enable checking if the player has achieved the expected outcome.\n\nGoldGoals checks if the Player´s gold amount has reached a given amount, HealthGoals checks if the Player´s health has reached a given amount, ScoreGoals checks if the Player´s score has reached a given amount and InventoryGoals checks if the Player has a given amount of items in their inventory.");
        helpMap.put("infoScene", "Here you are able to see information about this Story. The name of the Story and a short description of it.\n\nA list of dead Links, which are Links that don´t refer to any passages, and a list of dead Passages, which are Passages that arent referenced by any Links.\n\nYou also get the name of the file and the filepath.");
        helpMap.put("pickStory", "Here you can chose what game you want to play. You are able to upload any .paths file you want to play on.");
        helpMap.put("playerCreation", "Here you are able to create a Player for the game. You can give your character a name, starting health and gold amount (must be more than 0) and a List of items that will start in their inventory");
        helpMap.put("inventory", "Here you are able to add items to the inventory of your character. Just enter in a word, and add it to the inventory. Also possible to remove items.");
    }

    /**
     * Method for getting a specific
     * value from the Map.
     *
     * @param key the key to the value you want to get.
     * @throws NullPointerException if there is no matching key in the Map.
     * @return the value to the given key.
     */
    public static String getValue(String key) throws NullPointerException{
        if(helpMap.get(key) == null){
            throw new NullPointerException("There is no matching key in the Map");
        } else{
            return helpMap.get(key);
        }
    }
}
