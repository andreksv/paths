package edu.ntnu.idatt2001.paths.presentation;

import edu.ntnu.idatt2001.paths.businessLogic.Story;
import edu.ntnu.idatt2001.paths.presentation.popup.HelpPopup;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import static edu.ntnu.idatt2001.paths.presentation.CreateNode.*;

/**
 * Class that represents an info-scene.
 *
 * @author Anders Høvik
 */
public class InfoScene {
   // The primary Stage
   private static Stage primaryStage;
    /**
     * The Story you want to view on this Scene
     */
   private static Story story;
    /**
     * The FilePath to the Story / .paths file.
     */
   private static String filePath;
    /**
     * Integer that decides the size
     * of Buttons on the Scene. The lower, the bigger
     * the Buttons are.
     */
   private static final int buttonSize = 40;

    /**
     * Integer that decides the size
     * of Texts on the Scene. The lower, the bigger
     * the Texts are.
     */
    private static final int textSize = 40;

    /**
     * Integer that decides the size
     * of ListViews on the Scene. The lower, the bigger
     * the ListViews are.
     */
    private static final int listviewSize = 90;

    /**
     * Integer that decides the size
     * of Labels on the Scene. The lower, the bigger
     * the Labels are.
     */
    private static final int labelSize = 60;

    /**
     * Method for setting scene.
     *
     * @param stage the Stage you want to set a scene for.
     */
    public static void setScene(Stage stage, Story story, String filePath){
        primaryStage = stage;
        InfoScene.story = story;
        InfoScene.filePath = filePath;
        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(InfoScene.infoSceneLayout());
        Scene scene = new Scene(borderPane, primaryStage.getScene().getWidth(), primaryStage.getScene().getHeight(), Color.DODGERBLUE);
        primaryStage.setScene(scene);
    }

   /**
     * The main-layout that connects the other layouts.
     */
   private static Node infoSceneLayout(){
       BorderPane root  = new BorderPane();

       root.setTop(getTopPane());
       root.setCenter(getCenterPane());
       root.setBottom(getBottomPane());
       return root;
   }

    /**
     * Method for getting the Pane that goes on
     * the top part of the Scene.
     *
     * @return BorderPane.
     */
   private static Pane getTopPane(){
       BorderPane topPane = new BorderPane();

       Text title = CreateNode.createText(story.getTitle(), textSize, primaryStage);
       title.setTextAlignment(TextAlignment.CENTER);
       Button goBack = CreateNode.createButton("Go back", click-> PickStory.setScene(primaryStage),buttonSize, primaryStage);
       Button help = CreateNode.createButton("Help", click-> HelpPopup.showHelpPopup("infoScene"),buttonSize, primaryStage);

       topPane.setCenter(title);
       topPane.setLeft(goBack);
       topPane.setRight(help);

       return topPane;
   }

    /**
     * Method for getting the Pane that goes on
     * the center part of the Scene.
     *
     * @return HBox.
     */
   private static Pane getCenterPane(){
       HBox leftPane1 = new HBox(20);
       VBox leftPane2 = new VBox(10);
       VBox deadLinkPane = new VBox(10);
       VBox deadPassagePane = new VBox(10);
       HBox deadListViews = new HBox(80);

       Text fileName = CreateNode.createText("File name: " + filePath.substring(filePath.lastIndexOf("/") + 1),buttonSize, primaryStage);
       fileName.setTextAlignment(TextAlignment.CENTER);
       Text filePath = CreateNode.createText("(Path to file: " + InfoScene.filePath,buttonSize+20, primaryStage);
       filePath.setTextAlignment(TextAlignment.CENTER);
       leftPane2.getChildren().addAll(fileName, filePath);
       leftPane2.setAlignment(Pos.CENTER);

       Label deadLinksLabel = createLabel("Dead Links", labelSize, primaryStage);
       ListView<String> linksView = CreateNode.createListView(FXCollections.observableList(story.getBrokenLinks()),"Dead Links", listviewSize, primaryStage);
       deadLinkPane.getChildren().addAll(deadLinksLabel, linksView);
       deadLinkPane.setAlignment(Pos.CENTER);

       Label deadPassageLabel = createLabel("Dead Passages", labelSize, primaryStage);
       ListView<String> passagesView = CreateNode.createListView(FXCollections.observableList(story.getBrokenPassages()), "Dead Passages", listviewSize, primaryStage);
       deadPassagePane.getChildren().addAll(deadPassageLabel, passagesView);
       deadPassagePane.setAlignment(Pos.CENTER);
       deadListViews.getChildren().addAll(deadLinkPane, deadPassagePane);
       deadListViews.setAlignment(Pos.CENTER);

       leftPane2.getChildren().addAll(deadListViews);
       leftPane1.getChildren().addAll(new Label(""), leftPane2);
       leftPane1.setAlignment(Pos.CENTER);

       return leftPane1;
   }

    /**
     * Method for getting the Pane that goes on
     * the bottom part of the Scene.
     *
     * @return HBox.
     */
   private static Pane getBottomPane() {
       HBox bottomLayout = new HBox();
       Button continueButton = CreateNode.createButton("Continue", click-> PlayerCreation.setScene(primaryStage) ,buttonSize, primaryStage);
       bottomLayout.getChildren().addAll(continueButton);
       bottomLayout.setAlignment(Pos.CENTER);
       return bottomLayout;
   }

    /**
     * Method for getting story.
     *
     * @return Story.
     */
    public static Story getStory() {
        return InfoScene.story;
    }

    /**
     * Method for setting story.
     *
     * @param story the Story you want to set story to.
     */
    public void setStory(Story story) {
        InfoScene.story = story;
    }
}
