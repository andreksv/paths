package edu.ntnu.idatt2001.paths.presentation;

import edu.ntnu.idatt2001.paths.presentation.popup.HelpPopup;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * The first scene of the Paths game.
 *
 * @author Andreas Kluge Svendsrud
 */
public class  MainMenu extends Application{

    private static Stage primaryStage;

    /**
     * Integer that decides the size
     * of Buttons on the Scene. The lower, the bigger
     * the Buttons are.
     */
    static int buttonSize = 40;
    /**
     * Integer that decides the size
     * of Texts on the Scene. The lower, the bigger
     * the Texts are.
     */
    static int textSize = 30;


    public static void main(String[] args) {
        launch();
    }
    @Override
    public void start(Stage stage) {
        primaryStage = stage;
        primaryStage.initStyle(StageStyle.UTILITY);
        primaryStage.setTitle("Paths");
        primaryStage.setScene(getMainMenuScene(stage));
        primaryStage.show();
    }

    public static Scene getMainMenuScene(Stage stage){
        primaryStage = stage;
        StackPane stackPane = new StackPane();
        Scene mainMenuScene;
        if(primaryStage.getScene() != null) {
            mainMenuScene = new Scene(stackPane, primaryStage.getScene().getWidth(), primaryStage.getScene().getHeight());
        } else {
            mainMenuScene = new Scene(stackPane, 720, 560);
        }
        primaryStage.setScene(mainMenuScene);

        Text titleText = CreateNode.createText("Paths", textSize, primaryStage);
        VBox buttonLayout = new VBox(15);

        // Button for picking a story
        Button buttonPickStory = CreateNode.createButton("Pick a Story!",actionEvent -> PickStory.setScene(primaryStage), buttonSize, primaryStage);

        // Button for creating a story
        Button buttonCreateStory = CreateNode.createButton("Create a Story!", actionEvent -> CreateStory.setScene(1, primaryStage), buttonSize, primaryStage);

        // Button for help
        Button buttonHelp = CreateNode.createButton("Help", actionEvent -> HelpPopup.showHelpPopup("mainMenu"), buttonSize, primaryStage);

        // Button for Exit
        Button buttonExit = CreateNode.createButton("Exit!", actionEvent -> primaryStage.close(), buttonSize, primaryStage);

        buttonLayout.getChildren().addAll(titleText,buttonPickStory, buttonCreateStory, buttonHelp, buttonExit);
        buttonLayout.setAlignment(Pos.CENTER);
        stackPane.getChildren().addAll(buttonLayout);
        mainMenuScene.setFill(Paint.valueOf("#FFFFFF"));
        return mainMenuScene;
    }
}
