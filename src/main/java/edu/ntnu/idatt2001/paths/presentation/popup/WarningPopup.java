package edu.ntnu.idatt2001.paths.presentation.popup;

import javafx.scene.control.Alert;

public class WarningPopup {
    public static void showHelpPopup(String message) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("WARNING");
        alert.setHeaderText("WARNING");
        alert.setContentText(message);
        alert.showAndWait();
    }
}
