package edu.ntnu.idatt2001.paths.presentation.popup;

import javafx.scene.control.Alert;

/**
 * Class for displaying a Popup
 * that shows helpful information
 * to the scene you are on.
 *
 * @author Andreas Kluge Svendsrud
 */
public class HelpPopup {
    public static void showHelpPopup(String title) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Help - " + title);
        alert.setHeaderText("Help - " + title);
        try {
            alert.setContentText(Help.getValue(title));
        } catch (NullPointerException ex) {
            alert.setContentText("There is currently no help text for this Scene, or there is an error in the code.");
        }
        alert.showAndWait();
    }
}
