package edu.ntnu.idatt2001.paths.presentation;

import edu.ntnu.idatt2001.paths.businessLogic.goals.Goal;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import edu.ntnu.idatt2001.paths.businessLogic.Game;
import edu.ntnu.idatt2001.paths.businessLogic.Link;
import edu.ntnu.idatt2001.paths.businessLogic.Passage;
import edu.ntnu.idatt2001.paths.businessLogic.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for playing through a Story
 * in a Scene. It displays the different
 * Passage´s of a Story.
 *
 * @author Andreas Kluge Svendsrud
 */
public class DisplayPassage {
    private static Game game;
    private static Player playerReset;
    private static Stage primaryStage;
    private static Passage currentPassage;

    /**
     * Integer that decides the size
     * of Buttons on the Scene. The lower, the bigger
     * the Buttons are.
     */
    static int buttonSize = 40;

    /**
     * Integer that decides the size
     * of Texts on the Scene. The lower, the bigger
     * the Texts are.
     */
    static int textSize = 40;

    /**
     * Integer that decides the size
     * of Labels on the Scene. The lower, the bigger
     * the Labels are.
     */
    static int labelSize = 60;

    /**
     * Integer that decides the size
     * of HyperLinks on the Scene. The lower, the bigger
     * the HyperLinks are.
     */
    static int hyperlinkSize = 60;

    /**
     * Integer that decides the size
     * of ProgressBar on the Scene. The lower, the bigger
     * the ProgressBar are.
     */
    static int progressbarSize = 4;

    /**
     * Integer that decides the size
     * of TextArea on the Scene. The lower, the bigger
     * the TextArea are.
     */
    static int textareaSize = 70;

    /**
     * Integer that decides the size
     * of ListViews on the Scene. The lower, the bigger
     * the ListViews are.
     */
    static int listviewSize = 100;

    /**
     * Method for setting scene,
     * depending on which page you are
     * on.
     *
     * @param stage the window you want to set a scene for.
     */
    public static void setScene(Stage stage, Game game) {
        if(currentPassage == null) {
            currentPassage = game.getStory().getOpeningPassage();
        }
        if(playerReset == null) {
            playerReset = new Player.Builder()
                    .name(game.getPlayer().getName())
                    .gold(game.getPlayer().getGold())
                    .health(game.getPlayer().getHealth())
                    .score(game.getPlayer().getScore())
                    .build();
        }
        primaryStage = stage;
        DisplayPassage.game = game;

        BorderPane borderPane = new BorderPane();
        borderPane.setTop(getTopPane(currentPassage));
        borderPane.setCenter(DisplayPassage.getCenterPane());
        borderPane.setBottom(DisplayPassage.getBottomPane());

        Scene scene = new Scene(borderPane, primaryStage.getScene().getWidth(), primaryStage.getScene().getHeight(), Color.DODGERBLUE);
        primaryStage.setScene(scene);
    }

    /**
     * Method for getting the Pane that goes on
     * the top part of the Scene.
     *
     * @return HBox.
     */
    private static Pane getTopPane(Passage passage) {
        HBox paneAdjustHorizontal = new HBox(30);
        VBox panePassage = new VBox(20);
        VBox paneLinks = new VBox(20);
        HBox paneLinkAndPassage = new HBox(20);

        TextArea passageTitleContent;
        if(passage.getContent().isEmpty()) {
            passageTitleContent = CreateNode.createTextArea(passage.getTitle(), textareaSize, primaryStage);
        } else {
            passageTitleContent = CreateNode.createTextArea(passage.getTitle()+"\n\n"+passage.getContent(), textareaSize, primaryStage);
        }

        paneAdjustHorizontal.getChildren().addAll(passageTitleContent, new Text(""));
        panePassage.getChildren().addAll(new Text(""), paneAdjustHorizontal);

        if(passage.hasLinks()) {
            paneLinks.getChildren().addAll(CreateNode.createLabel("You can ...", labelSize, primaryStage));
            for(Link link : passage.getLinks()) {
                Hyperlink hyperlink = CreateNode.createHyperlink(link, actionEvent -> {
                    currentPassage = game.go(link);
                    DisplayPassage.setScene(primaryStage, game);
                }, hyperlinkSize, primaryStage);

                paneLinks.getChildren().add(hyperlink);
                paneLinks.setAlignment(Pos.CENTER);
            }
        } else {
            Button buttonReset = CreateNode.createButton("Click here to Reset", actionEvent -> {
                currentPassage = game.begin();
                DisplayPassage.game.setPlayer(new Player.Builder()
                        .name(playerReset.getName())
                        .gold(playerReset.getGold())
                        .health(playerReset.getHealth())
                        .score(playerReset.getScore())
                        .build());
                DisplayPassage.setScene(primaryStage, game);
            }, buttonSize, primaryStage);

            paneLinks.getChildren().addAll(buttonReset);
            paneLinks.setAlignment(Pos.CENTER);
        }

        panePassage.setAlignment(Pos.CENTER);
        paneLinkAndPassage.getChildren().addAll(panePassage, paneLinks);
        paneLinkAndPassage.setAlignment(Pos.CENTER);
        return paneLinkAndPassage;
    }

    /**
     * Method for getting the Pane that goes on
     * the center part of the Scene.
     *
     * @return HBox.
     */
    private static Pane getCenterPane() {
        HBox hBox = new HBox(50);

        Text textGold = CreateNode.createText("Gold amount: " + game.getPlayer().getGold(), textSize, primaryStage);
        Text textScore = CreateNode.createText("Score amount: " + game.getPlayer().getScore(), textSize, primaryStage);
        Button buttonInventory = CreateNode.createButton("Inventory", actionEvent -> {
            Stage inventoryStage = new Stage();
            VBox inventoryItems = new VBox(20);
            Scene inventoryScene = new Scene(inventoryItems, primaryStage.getScene().getWidth() / 1.5, (primaryStage.getScene().getHeight() / 1.5));
            inventoryStage.setScene(inventoryScene);
            inventoryItems.getChildren().add(CreateNode.createText("Your Inventory:", textSize-10, inventoryStage));

            inventoryStage.initModality(Modality.APPLICATION_MODAL);
            inventoryStage.initOwner(primaryStage);
            if(!game.getPlayer().getInventory().isEmpty()) {
                for(String item : game.getPlayer().getInventory()) {
                    inventoryItems.getChildren().add(CreateNode.createText(item, textSize-10, inventoryStage));
                }
            } else {
                inventoryItems.getChildren().add(CreateNode.createText("Empty inventory", textSize-10, inventoryStage));
            }
            inventoryItems.getChildren().add(CreateNode.createButton("CLOSE", actionEvent1 -> inventoryStage.close(), buttonSize, inventoryStage));
            inventoryItems.setAlignment(Pos.CENTER);
            inventoryStage.show();
        }, buttonSize, primaryStage);

        hBox.getChildren().addAll(buttonInventory, textScore, textGold);
        hBox.setAlignment(Pos.CENTER);
        return hBox;
    }

    /**
     * Method for getting the Pane that goes on
     * the bottom part of the Scene.
     *
     * @return BorderPane.
     */
    private static Pane getBottomPane() {
        VBox paneHealth = new VBox();
        StackPane paneHealthText = new StackPane();
        BorderPane paneBottom = new BorderPane();

        double healthPercentage = (double) game.getPlayer().getHealth() / (double) playerReset.getHealth();
        ProgressBar healthBar = CreateNode.createProgressBar(healthPercentage, progressbarSize, primaryStage);
        Text health = CreateNode.createText(game.getPlayer().getHealth() + "hp / " + game.getPlayer().getHealth() + "hp" , textSize, primaryStage);
        paneHealthText.getChildren().addAll(healthBar, health);
        paneHealthText.setAlignment(Pos.CENTER);
        Text playerName = CreateNode.createText(game.getPlayer().getName(), textSize, primaryStage);

        final Stage menuStage = new Stage();
        menuStage.setScene(new Scene(new Pane(), primaryStage.getWidth() / 2, primaryStage.getHeight() / 2 ));
        Text textMenu = CreateNode.createText("MENU", textSize, menuStage);
        Button buttonReset = CreateNode.createButton("Reset", actionEvent -> {
            currentPassage = game.begin();
            DisplayPassage.game.setPlayer(new Player.Builder()
                    .name(playerReset.getName())
                    .gold(playerReset.getGold())
                    .health(playerReset.getHealth())
                    .score(playerReset.getScore())
                    .build());
            setScene(primaryStage, game);
            menuStage.hide();
        }, buttonSize, menuStage);
        Button buttonMainMenu = CreateNode.createButton("Main menu", actionEvent -> {
            primaryStage.setScene(MainMenu.getMainMenuScene(primaryStage));
            currentPassage = game.begin();
            menuStage.hide();
        }, buttonSize, menuStage);
        Button buttonReturn = CreateNode.createButton("Return", actionEvent -> {
            menuStage.hide();
        }, buttonSize, menuStage);
        Button buttonMenu = CreateNode.createButton("Menu", actionEvent -> {
            if(menuStage.getModality() != Modality.APPLICATION_MODAL) {
                menuStage.initModality(Modality.APPLICATION_MODAL);
                menuStage.initOwner(primaryStage);
            }
            VBox buttonsVertical = new VBox(20);
            HBox buttonsHorizontal = new HBox(30);
            buttonsHorizontal.getChildren().addAll(buttonReset, buttonMainMenu);
            buttonsVertical.getChildren().addAll(textMenu, buttonsHorizontal, buttonReturn);
            buttonsHorizontal.setAlignment(Pos.CENTER);
            buttonsVertical.setAlignment(Pos.CENTER);
            Scene menuScene = new Scene(buttonsVertical, primaryStage.getWidth() / 2, primaryStage.getHeight() / 2);
            menuStage.setScene(menuScene);
            menuStage.showAndWait();
        }, buttonSize + 8, primaryStage);
        Button buttonShowGoals = CreateNode.createButton("Show Goals", actionEvent -> {
            if(menuStage.getModality() != Modality.APPLICATION_MODAL) {
                menuStage.initModality(Modality.APPLICATION_MODAL);
                menuStage.setResizable(false);
                menuStage.initOwner(primaryStage);
            }
            VBox buttonsVertical = new VBox(20);

            Text text = CreateNode.createText("GOALS", textSize, primaryStage);

            List<String> goalsOrganized = new ArrayList<>();
            for(Goal aGoal : game.getGoals()) {
                if(game.getFulfilledGoals().contains(aGoal)) {
                    goalsOrganized.add(aGoal + " - COMPLETED");
                } else {
                    goalsOrganized.add(aGoal.toString());
                }
            }
            ListView<String> goalListView = CreateNode.createListView(goalsOrganized, "Goals", listviewSize, primaryStage);
            buttonsVertical.getChildren().addAll(text, goalListView, buttonReturn);
            buttonsVertical.setAlignment(Pos.CENTER);
            Scene menuScene = new Scene(buttonsVertical, primaryStage.getWidth() / 2, primaryStage.getHeight() / 2);
            menuStage.setScene(menuScene);
            menuStage.showAndWait();
        }, buttonSize + 8, primaryStage);
        BorderPane borderPaneButtonMenu = new BorderPane();
        borderPaneButtonMenu.setBottom(buttonMenu);

        BorderPane borderPaneButtonGoals = new BorderPane();
        borderPaneButtonGoals.setBottom(buttonShowGoals);

        paneHealth.getChildren().addAll(playerName, paneHealthText);
        paneHealth.setAlignment(Pos.CENTER);
        paneBottom.setLeft(borderPaneButtonMenu);
        paneBottom.setRight(borderPaneButtonGoals);
        paneBottom.setCenter(paneHealth);
        return paneBottom;
    }
}
