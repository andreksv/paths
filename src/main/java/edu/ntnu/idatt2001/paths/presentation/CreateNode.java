package edu.ntnu.idatt2001.paths.presentation;

import edu.ntnu.idatt2001.paths.businessLogic.Link;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
 * Classing for creating different Nodes
 * for the GUI. Different nodes will have the same
 * size and scaling abilities.
 *
 * @author Andreas Kluge Svendsrud
 */
public class CreateNode {

    /**
     * Method for getting the
     * average size of a Stage divided
     * by a given Integer.
     *
     * @param ratio the Integer you want to divide with.
     * @param window the Stage you want to get the average size from.
     * @return the average size of window divided by ratio.
     */
    public static double getSize(int ratio, Stage window) {
        return (window.getScene().getHeight() + window.getScene().getWidth()) / ratio;
    }
    /**
     * Method for creating a Button.
     *
     * @param text the text you want to have on the Button.
     * @param eventHandler what you want the button to do when you click on it.
     * @param ratio int that decides the size of the Button compared to Stage.
     * @param window the Stage you will proportion the button according to.
     * @return Button
     */
    public static Button createButton(String text, EventHandler<ActionEvent> eventHandler, int ratio, Stage window) {
        Button button = new Button(text);
        button.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;");
        window.widthProperty().addListener((obs, oldVal, newVal) -> button.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;"));
        window.heightProperty().addListener((obs, oldVal, newVal) -> button.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;"));

        button.setOnAction(eventHandler);
        return button;
    }

    /**
     * Method for creating Text.
     *
     * @param string the String/text of your text.
     * @param ratio the ratio in size between window and the Text.
     * @param window the Stage you will proportion the Text according to.
     * @return Text
     */
    public static Text createText(String string, int ratio, Stage window) {
        Text text = new Text(string);
        text.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;");
        window.widthProperty().addListener((obs, oldVal, newVal) -> text.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;"));
        window.heightProperty().addListener((obs, oldVal, newVal) -> text.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;"));
        return text;
    }

    /**
     * Method for creating a Label.
     *
     * @param title the text of the Label
     * @param ratio the ratio in size between window and the Label.
     * @param window the Stage you will proportion the Label according to.
     * @return Label
     */
    public static Label createLabel(String title, int ratio, Stage window) {
        Label label = new Label(title);
        label.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;");

        window.widthProperty().addListener((obs, oldVal, newVal) -> label.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;"));
        window.heightProperty().addListener((obs, oldVal, newVal) -> label.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;"));
        return label;
    }

    /**
     * Method for creating a Hyperlink.
     *
     * @param link the Link you want this Hyperlink to have.
     * @param eventHandler the ActionEven you want to happen when you click on the Hyperlink.
     * @param ratio the ratio in size between window and the Text.
     * @param window the Stage you will proportion the ListView according to.
     * @return Hyperlink.
     */
    public static Hyperlink createHyperlink(Link link, EventHandler<ActionEvent> eventHandler, int ratio, Stage window) {
        Hyperlink hyperlink = new Hyperlink(link.getText());
        hyperlink.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;");

        window.widthProperty().addListener((obs, oldVal, newVal) -> hyperlink.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;"));
        window.heightProperty().addListener((obs, oldVal, newVal) -> hyperlink.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;"));
        hyperlink.setOnAction(eventHandler);
        return hyperlink;
    }

    /**
     * Method for creating a ListView.
     *
     * @param list the List you want to display in ListView
     * @param content a String that represents what the ListView contains
     * @param window the Stage you will proportion the ListView according to.
     * @param ratio the ratio in size between window and the Text.
     * @param <T> The object the list contains.
     * @return ListView
     */
    public static <T> ListView<String> createListView(List<T> list, String content, int ratio, Stage window) {
        ObservableList<String> observableList = FXCollections.observableArrayList();
        ListView<String> listView = new ListView<>(observableList);
        if (!list.isEmpty()) {
            for (T t : list) {
                observableList.add("- " + t.toString());
            }
        } else {
            observableList.add("No " + content);
        }
        listView.prefHeightProperty().bind(Bindings.size(observableList).multiply(window.getScene().getHeight() / 2));
        listView.setMinWidth(window.getScene().getWidth() / 3);
        listView.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;");

        window.widthProperty().addListener((obs, oldVal, newVal) -> {
            listView.prefHeightProperty().bind(Bindings.size(observableList).multiply(window.getScene().getHeight() / 2));
            listView.setMinWidth(window.getScene().getWidth() / 3);
            listView.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;");
        });
        window.heightProperty().addListener((obs, oldVal, newVal) -> {
            listView.prefHeightProperty().bind(Bindings.size(observableList).multiply(window.getScene().getHeight() / 2));
            listView.setMinWidth(window.getScene().getWidth() / 3);
            listView.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;");
        });
        return listView;
    }

    /**
     * Method for creating a TextField that take
     * in a String as a variable.
     *
     * @param promptText the PromptText you want to set for your TextField.
     * @param variable the Object that will be updated based on the TextField.
     * @param ratio the ratio in size between window and the TextField.
     * @param window the Stage you will proportion the TextField according to.
     * @return TextField
     */
    public static TextField createTextField(String promptText, Object variable, int ratio, Stage window) {
        TextField textField = new TextField();
        textField.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;");
        textField.setMaxWidth(window.getWidth() / 2.75);
        window.widthProperty().addListener((obs, oldVal, newVal) -> {
            textField.setMaxWidth(window.getWidth() / 2.75);
            textField.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;");
        });
        window.heightProperty().addListener((obs, oldVal, newVal) -> {
            textField.setMaxWidth(window.getWidth() / 2.75);
            textField.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;");
        });

        if (variable instanceof String && ((String) variable).isBlank()) {
            textField.setPromptText(promptText);
        } else if (variable instanceof String) {
            textField.setText((String) variable);
        } else if (variable instanceof Integer && (int) variable == 0) {
            textField.setPromptText(promptText);
        } else if (variable instanceof Integer) {
            textField.setText(String.valueOf((int) variable));
        }

        textField.setAlignment(Pos.CENTER);
        return textField;
    }

    /**
     * Method for creating a ProgressBar.
     *
     * @param progress a double representing the progress of progressbar
     * @param ratio the ratio in size between window and the ProgressBar.
     * @param window the Stage you will proportion the ListView according to.
     * @return ProgressBar
     */
    public static ProgressBar createProgressBar(double progress, int ratio, Stage window) {
        ProgressBar progressBar = new ProgressBar(progress);
        progressBar.setMaxWidth(window.getWidth() - (window.getWidth() / ratio));
        progressBar.setMinHeight(window.getWidth() / (ratio * 4));

        window.widthProperty().addListener((obs, oldVal, newVal) -> {
            progressBar.setMaxWidth(window.getWidth() - (window.getWidth() / ratio));
            progressBar.setMinHeight(window.getWidth() / (ratio * 4));
        });
        window.heightProperty().addListener((obs, oldVal, newVal) -> {
            progressBar.setMaxWidth(window.getWidth() - (window.getWidth() / ratio));
            progressBar.setMinHeight(window.getWidth() / (ratio * 4));
        });
        progressBar.setStyle("-fx-accent: orangered;");
        return progressBar;
    }

    /**
     * Method for creating a TextArea.
     * Not possible to edit.
     *
     * @param text the text you want to have inside the TextArea.
     * @param ratio the ratio in size between window and the ProgressBar.
     * @param window the Stage you will proportion the ListView according to.
     * @return TextArea
     */
    public static TextArea createTextArea(String text, int ratio, Stage window) {
        TextArea textArea = new TextArea(text);
        textArea.setEditable(false);
        textArea.setMaxWidth(window.getWidth() / 2);
        textArea.setMaxHeight(window.getHeight() / 3);
        textArea.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;");

        window.widthProperty().addListener((obs, oldVal, newVal) -> {
            textArea.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;");
            textArea.setMaxWidth(window.getWidth() / 2);
            textArea.setMaxHeight(window.getHeight() / 3);
        });
        window.heightProperty().addListener((obs, oldVal, newVal) -> {
            textArea.setStyle("-fx-font-size: " + getSize(ratio, window) + "px;");
            textArea.setMaxWidth(window.getWidth() / 2);
            textArea.setMaxHeight(window.getHeight() / 3);
        });
        textArea.setWrapText(true);
        return textArea;
    }
}
