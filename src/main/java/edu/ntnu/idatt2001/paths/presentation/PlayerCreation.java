package edu.ntnu.idatt2001.paths.presentation;

import edu.ntnu.idatt2001.paths.businessLogic.Player;
import edu.ntnu.idatt2001.paths.presentation.popup.ErrorPopup;
import edu.ntnu.idatt2001.paths.presentation.popup.HelpPopup;
import edu.ntnu.idatt2001.paths.presentation.popup.WarningPopup;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.text.*;

import java.util.ArrayList;
import java.util.List;

import static edu.ntnu.idatt2001.paths.presentation.CreateNode.*;
import static edu.ntnu.idatt2001.paths.presentation.CreateNode.createTextField;

/**
 * Class for getting the Scene
 * for creating a Player-object.
 *
 * @author Anders Høvik
 */
public class PlayerCreation {
    // The primary Stage
    private static Stage primaryStage;
    // Player object that is created on this Scene
    private static Player player;

    /**
     * Integer that decides the size
     * of Buttons on the Scene. The lower, the bigger
     * the Buttons are.
     */
    static int buttonSize = 40;

    /**
     * Integer that decides the size
     * of Texts on the Scene. The lower, the bigger
     * the Texts are.
     */
    static int textSize = 40;

    /**
     * Integer that decides the size
     * of Labels on the Scene. The lower, the bigger
     * the Labels are.
     */
    static int labelSize = 40;

    /**
     * Integer that decides the size
     * of ListViews on the Scene. The lower, the bigger
     * the ListViews are.
     */
    static int listviewSize = 100;

    /**
     * Integer that decides the size
     * of TextFields on the Scene. The lower, the bigger
     * the TextFields are.
     */
    static int textfieldSize = 80;

    // Player name variables
    private static TextField namePlayer = new TextField();
    private static String name = "";
    // Player health variables
    private static TextField healthPlayer = new TextField();
    private static int health;
    // Player gold variables
    private static TextField goldPlayer = new TextField();
    private static int gold;
    // Player inventory variables
    private static ListView inventoryPlayer = new ListView();
    private static List<String> inventory = new ArrayList<>();

    /**
     * Method for setting scene.
     *
     * @param stage the window you want to set a scene for.
     */
    public static void setScene(Stage stage) {
        primaryStage = stage;
        BorderPane borderPane = new BorderPane();

        borderPane.setTop(getTopPane());
        borderPane.setCenter(getCenterPane());
        borderPane.setBottom(getBottomPane());
        Scene scene = new Scene(borderPane, primaryStage.getScene().getWidth(), primaryStage.getScene().getHeight());
        primaryStage.setScene(scene);
    }

    /**
     * Method for getting the Pane that goes on
     * the top part of the Scene.
     *
     * @return BorderPane.
     */
    private static Pane getTopPane() {
        BorderPane topPane = new BorderPane();

        Button goBack = CreateNode.createButton("Go back", actionEvent -> PickStory.setScene(primaryStage), buttonSize, primaryStage);
        Text enterYourCharacter = CreateNode.createText("Enter your character:", textSize, primaryStage);
        enterYourCharacter.setTextAlignment(TextAlignment.CENTER);
        Button help = CreateNode.createButton("Help", actionEvent -> HelpPopup.showHelpPopup("playerCreation"), buttonSize, primaryStage);

        topPane.setLeft(goBack);
        topPane.setCenter(enterYourCharacter);
        topPane.setRight(help);

        return topPane;
    }

    /**
     * Method for getting the Pane that goes on
     * the center part of the Scene.
     *
     * @return VBox.
     */
    private static Pane getCenterPane() {
        VBox centerPane = new VBox(20);

        namePlayer = CreateNode.createTextField("Enter your character name", name, textfieldSize, primaryStage);
        healthPlayer = CreateNode.createTextField("Enter your starting health", health, textfieldSize, primaryStage);
        goldPlayer = CreateNode.createTextField("Enter your starting gold", gold, textfieldSize, primaryStage);
        Button inventoryB = CreateNode.createButton("Inventory", actionEvent -> {
            name = namePlayer.getText();
            try {
                health = Integer.parseInt(healthPlayer.getText());
                gold = Integer.parseInt(goldPlayer.getText());
            } catch(NumberFormatException ignored){}

            primaryStage.setScene(getInventoryScene());
        }, buttonSize, primaryStage);

        centerPane.getChildren().addAll(namePlayer, healthPlayer, goldPlayer, inventoryB);
        centerPane.setAlignment(Pos.CENTER);

        return centerPane;
    }

    /**
     * Method for getting the Pane that goes on
     * the bottom part of the Scene.
     *
     * @return HBox.
     */
    private static Pane getBottomPane() {
        HBox bottomPane = new HBox();

        Button continueB = CreateNode.createButton("Continue", actionEvent -> {
            if(namePlayer.getText().isEmpty()) {
                name = "Hero";
            } else {
                name = namePlayer.getText();
            }
            try {
                if (healthPlayer.getText().isEmpty()) {
                    health = 100;
                } else {
                    health = Integer.parseInt(healthPlayer.getText());
                }
                if (goldPlayer.getText().isEmpty()) {
                    gold = 0;
                } else {
                    gold = Integer.parseInt(goldPlayer.getText());
                }

                player = new Player.Builder()
                        .name(name)
                        .health(health)
                        .gold(gold)
                        .inventory(inventory)
                        .build();

                GoalSelection.setScene(primaryStage);
            } catch(IllegalArgumentException ex) {
                ErrorPopup.showErrorPopup(ex.getMessage(), ex.getClass().getSimpleName());
            }
        },buttonSize, primaryStage);

        bottomPane.getChildren().add(continueB);
        bottomPane.setAlignment(Pos.CENTER);

        return bottomPane;
    }

    /**
     * Method for getting the
     * Scene for editing
     * the inventory of
     * the Player
     *
     * @return Scene
     */
    private static Scene getInventoryScene() {
        BorderPane inventoryPane = new BorderPane();
        Scene inventoryScene = new Scene(inventoryPane, primaryStage.getScene().getWidth(), (primaryStage.getScene().getHeight()));

        BorderPane layoutTop = new BorderPane();

        // Top layout
        Text textTitle = CreateNode.createText("Inventory", textSize, primaryStage);
        textTitle.setTextAlignment(TextAlignment.CENTER);
        Button buttonBack = createButton("Go back", click -> setScene(primaryStage), buttonSize, primaryStage);
        Button buttonHelp = createButton("Help", click -> HelpPopup.showHelpPopup("inventory"),buttonSize, primaryStage);
        layoutTop.setLeft(buttonBack);
        layoutTop.setRight(buttonHelp);
        layoutTop.setCenter(textTitle);

        // Right layout
        VBox listLayout = new VBox(10);
        Label labelEmpty = createLabel("", labelSize, primaryStage);
        Label label = createLabel("List of Items:", labelSize, primaryStage);
        inventoryPlayer = createListView(inventory, "Items", listviewSize, primaryStage);
        Button buttonRemoveItem = CreateNode.createButton("Remove Item", actionEvent -> {
            try {
                int selectedIndex = inventoryPlayer.getSelectionModel().getSelectedIndex();
                if (selectedIndex != -1) {
                    inventory.remove(selectedIndex);
                    primaryStage.setScene(getInventoryScene());
                } else {
                    WarningPopup.showHelpPopup("You must select an Item!");
                }
            } catch(IndexOutOfBoundsException ex) {
                ErrorPopup.showErrorPopup("You must add an Item, to be able to remove.", "No Items in List");
            }

        }, buttonSize, primaryStage);
        listLayout.getChildren().addAll(labelEmpty,label, inventoryPlayer, buttonRemoveItem);

        // Center layout
        String stringAction = "";
        TextField itemTextField = createTextField("<Enter item>", stringAction, textfieldSize+20, primaryStage);
        Button buttonAdd = CreateNode.createButton("Add Item", actionEvent2 -> {
            if (itemTextField.getText().isBlank()) {
                WarningPopup.showHelpPopup("You must enter a value for your Item!");
            } else {
                inventory.add(itemTextField.getText());
                primaryStage.setScene(getInventoryScene());
            }
        }, buttonSize + 10, primaryStage);
        VBox centerLayout = new VBox(10);
        centerLayout.getChildren().addAll(itemTextField, buttonAdd);
        centerLayout.setAlignment(Pos.CENTER);

        // Bottom layout
        Button buttonInventoryGoal = CreateNode.createButton("Go Back", actionEvent2 -> {
            setScene(primaryStage);
        }, buttonSize, primaryStage);
        HBox bottomLayout = new HBox(20);
        bottomLayout.getChildren().add(buttonInventoryGoal);
        bottomLayout.setAlignment(Pos.CENTER);

        inventoryPane.setTop(layoutTop);
        inventoryPane.setCenter(centerLayout);
        inventoryPane.setRight(listLayout);
        inventoryPane.setBottom(bottomLayout);
        return inventoryScene;
    }

    /**
     * Method for getting
     * player
     *
     * @return Player
     */
    public static Player getPlayer() {
        return player;
    }

}

