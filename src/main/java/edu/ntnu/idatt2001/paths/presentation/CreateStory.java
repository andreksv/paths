package edu.ntnu.idatt2001.paths.presentation;

import edu.ntnu.idatt2001.paths.businessLogic.Link;
import edu.ntnu.idatt2001.paths.businessLogic.Passage;
import edu.ntnu.idatt2001.paths.businessLogic.Story;
import edu.ntnu.idatt2001.paths.data.FileHandling;
import edu.ntnu.idatt2001.paths.presentation.popup.ErrorPopup;
import edu.ntnu.idatt2001.paths.presentation.popup.HelpPopup;
import edu.ntnu.idatt2001.paths.presentation.popup.WarningPopup;
import edu.ntnu.idatt2001.paths.businessLogic.actions.*;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static edu.ntnu.idatt2001.paths.presentation.CreateNode.*;

/**
 * Class for getting Scenes for creating
 * a Story from the GUI
 *
 * @author Andreas Kluge Svendsrud
 */
public class CreateStory {

    // The primary Stage
    private static Stage primaryStage;

    // All Story variables.
    private static Story story;
    private static TextField titleStory = new TextField();

    // All Passage variables.
    private static Passage passage;
    private static TextField titlePassage = new TextField();
    private static TextField contentPassage = new TextField();
    private static final List<Passage> passageList = new ArrayList<>();

    // All Link variables.
    private static Link link;
    private static TextField textLink = new TextField();
    private static TextField referenceLink = new TextField();
    private static final List<Link> linkList = new ArrayList<>();

    // All Action variables.
    private static Action action;
    private static TextField actionAmount = new TextField();
    private static final List<Action> actionList = new ArrayList<>();
    private static boolean healthAction;
    private static boolean goldAction;
    private static boolean scoreAction;
    private static boolean inventoryAction;


    // Constants
    /**
     * Map that has Integers from 1-4 as Key´s
     * to signify which Scene you are on. Scene can either
     * be for Creating a Story, Passage, Link or
     * Action.
     */
    private static final Map<Integer, String> pageNumberMap;
    static {
        pageNumberMap = new HashMap<>();
        pageNumberMap.put(1, "Story");
        pageNumberMap.put(2, "Passage");
        pageNumberMap.put(3, "Link");
        pageNumberMap.put(4, "Action");
    }

    /**
     * Integer that decides the size
     * of Buttons on the Scene. The lower, the bigger
     * the Buttons are.
     */
    private static final int buttonSize = 40;

    /**
     * Integer that decides the size
     * of Texts on the Scene. The lower, the bigger
     * the Texts are.
     */
    private static final int textSize = 40;

    /**
     * Integer that decides the size
     * of Labels on the Scene. The lower, the bigger
     * the Labels are.
     */
    private static final int labelSize = 40;

    /**
     * Integer that decides the size
     * of ListViews on the Scene. The lower, the bigger
     * the ListViews are.
     */
    private static final int listviewSize = 100;

    /**
     * Integer that decides the size
     * of TextFields on the Scene. The lower, the bigger
     * the TextFields are.
     */
    private static final int textfieldSize = 80;

    /**
     * Method for setting scene,
     * depending on which page you are
     * on.
     *
     * @param pageNumber int from 1-4 representing which page you are on.
     * @param stage the window you want to set a scene for.
     */
    public static void setScene(int pageNumber, Stage stage) {
        primaryStage = stage;
        BorderPane borderPane = new BorderPane();
        borderPane.setTop(CreateStory.getTopPane(pageNumber));
        borderPane.setCenter(CreateStory.getCenterPane(pageNumber));
        borderPane.setRight(CreateStory.getRightPane(pageNumber));
        borderPane.setBottom(CreateStory.getBottomPane(pageNumber));
        Scene scene = new Scene(borderPane, primaryStage.getScene().getWidth(), primaryStage.getScene().getHeight());
        primaryStage.setScene(scene);
    }

    /**
     * Method for getting the Pane that goes on
     * the top part of the Scene.
     *
     * @param pageNumber the int value for the page you are on.
     * @return BorderPane.
     */
    private static Pane getTopPane(int pageNumber){
        BorderPane layoutTop = new BorderPane();

        // Text title
        Text textTitle = CreateNode.createText("Create-A-"+ pageNumberMap.get(pageNumber), textSize, primaryStage);
        Text textSubTitle = CreateNode.createText("(Create your own "+ pageNumberMap.get(pageNumber)+"!)", textSize, primaryStage);
        VBox layoutText = new VBox();
        layoutText.getChildren().addAll(textTitle, textSubTitle);
        layoutText.setAlignment(Pos.CENTER);

        // Button for going back and Help
        Button buttonBack = createButton("Go back", click -> {if(pageNumber == 1){
            primaryStage.setScene(MainMenu.getMainMenuScene(primaryStage));} else{CreateStory.setScene(pageNumber-1, primaryStage);}}, buttonSize, primaryStage);
        Button buttonHelp = createButton("Help", click -> HelpPopup.showHelpPopup("create"+ pageNumberMap.get(pageNumber)),buttonSize, primaryStage);

        layoutTop.setLeft(buttonBack);
        layoutTop.setRight(buttonHelp);
        layoutTop.setCenter(layoutText);
        return layoutTop;
    }

    /**
     * Method for getting the Pane that goes on
     * the center part of the Scene.
     *
     * @param pageNumber the int value for the page you are on.
     * @return BorderPane.
     */
    private static Pane getCenterPane(int pageNumber) {
        VBox layoutCenter = new VBox(30);
        StackPane stackPane = new StackPane();

        if(pageNumber == 1) {
            titleStory = createTextField("<Enter title of Story>", titleStory.getText(), textfieldSize, primaryStage);

            layoutCenter.getChildren().addAll(titleStory);
            layoutCenter.setAlignment(Pos.CENTER);
        } else if(pageNumber == 2) {
            titlePassage = createTextField("<Enter title of Passage>", titlePassage.getText(), textfieldSize, primaryStage);
            contentPassage = createTextField("<Enter content of Passage>", contentPassage.getText(), textfieldSize, primaryStage);

            layoutCenter.getChildren().addAll(titlePassage, contentPassage);
            layoutCenter.setAlignment(Pos.CENTER);
        } else if(pageNumber == 3) {
            textLink = createTextField("<Enter text of Link>", textLink.getText(), textfieldSize, primaryStage);
            referenceLink = createTextField("<Enter reference of Link>", referenceLink.getText(), textfieldSize, primaryStage);

            layoutCenter.getChildren().addAll(textLink, referenceLink);
            layoutCenter.setAlignment(Pos.CENTER);
        } else if(pageNumber == 4) {
            if(healthAction) {
                actionAmount = createTextField("<Enter health amount>", "", textfieldSize, primaryStage);
                layoutCenter.getChildren().addAll(actionAmount);
            } else if (goldAction) {
                actionAmount = createTextField("<Enter gold amount>", "", textfieldSize, primaryStage);
                layoutCenter.getChildren().addAll(actionAmount);
            } else if (scoreAction) {
                actionAmount = createTextField("<Enter score amount>", "", textfieldSize, primaryStage);
                layoutCenter.getChildren().addAll(actionAmount);
            } else if (inventoryAction) {
                actionAmount = createTextField("<Enter item>", "", textfieldSize, primaryStage);
                layoutCenter.getChildren().addAll(actionAmount);
            }
        }
        layoutCenter.setAlignment(Pos.CENTER);
        stackPane.getChildren().add(layoutCenter);
        return stackPane;
    }

    /**
     * Method for getting the Pane that goes on
     * the right part of the Scene.
     *
     * @param pageNumber the int value for the page you are on.
     * @return BorderPane.
     */
    private static Pane getRightPane(int pageNumber) {
        VBox layoutRight = new VBox(15);

        /*
          If it is on the
          Create-A-Story scene. Will have a
          List of all Passages added so far
          to the Story, and a button for adding
          more Passages
         */
        if(pageNumber == 1) {
            // List of Links in the Story
            Label labelEmpty = createLabel("", labelSize, primaryStage);
            Label label = createLabel("List of Passages:", labelSize, primaryStage);
            ListView<String> listViewPassages = createListView(passageList, "Passages", listviewSize, primaryStage);
            Button buttonRemovePassages = CreateNode.createButton("Remove Passage", actionEvent -> {
                try {
                    int selectedIndex = listViewPassages.getSelectionModel().getSelectedIndex();
                    if (selectedIndex != -1) {
                        passageList.remove(selectedIndex);
                        setScene(pageNumber, primaryStage);
                    } else {
                        WarningPopup.showHelpPopup("You must select a Passage!");
                    }
                } catch(IndexOutOfBoundsException ex) {
                    WarningPopup.showHelpPopup("There are no Passages to be removed");
                }
            }, buttonSize, primaryStage);
            Button buttonAddPassage = createButton("Add "+ pageNumberMap.get(pageNumber+1), click -> CreateStory.setScene(pageNumber+1, primaryStage), buttonSize, primaryStage);

            layoutRight.getChildren().addAll(labelEmpty,label, listViewPassages, buttonRemovePassages, buttonAddPassage);
            layoutRight.setAlignment(Pos.CENTER);
        }
        /*
          If it is on the
          Create-A-Passage scene. Will have a
          List of all Links added so far
          to the Passage, and a button for adding
          more Links.
         */
        else if(pageNumber == 2) {
            // List of Links in the Passage
            Label labelEmpty = createLabel("",labelSize, primaryStage);
            Label label = createLabel("List of Links:", labelSize, primaryStage);
            ListView<String> listViewLinks = createListView(linkList, "Links", listviewSize, primaryStage);
            Button buttonRemoveLink = CreateNode.createButton("Remove Link", actionEvent -> {
                try {
                    int selectedIndex = listViewLinks.getSelectionModel().getSelectedIndex();
                    if (selectedIndex != -1) {
                        linkList.remove(selectedIndex);
                        setScene(pageNumber, primaryStage);
                    } else {
                        WarningPopup.showHelpPopup("You must select a Link!");
                    }
                } catch(IndexOutOfBoundsException ex) {
                    WarningPopup.showHelpPopup("There are no Links to be removed");
                }
            }, buttonSize, primaryStage);
            Button buttonAddPassage = createButton("Add "+ pageNumberMap.get(pageNumber+1), click -> CreateStory.setScene(pageNumber+1, primaryStage), buttonSize, primaryStage);

            layoutRight.getChildren().addAll(labelEmpty,label, listViewLinks, buttonRemoveLink, buttonAddPassage);
            layoutRight.setAlignment(Pos.CENTER);
        }
        /*
          If it is on the
          Create-A-Link scene. Will have a
          List of all Actions added so far
          to the Story, and a button for adding
          more Actions.
         */
        else if(pageNumber == 3) {
            // List of Actions in the Link
            Label labelEmpty = createLabel("", labelSize, primaryStage);
            Label label = createLabel("List of Actions:", labelSize, primaryStage);
            ListView<String> listViewActions = createListView(actionList, "Actions", listviewSize, primaryStage);
            Button buttonRemoveAction = CreateNode.createButton("Remove Action", actionEvent -> {
                try {
                    int selectedIndex = listViewActions.getSelectionModel().getSelectedIndex();
                    if (selectedIndex != -1) {
                        actionList.remove(selectedIndex);
                        setScene(pageNumber, primaryStage);
                    } else {
                        WarningPopup.showHelpPopup("You must select an Action!");
                    }
                } catch(IndexOutOfBoundsException ex) {
                    WarningPopup.showHelpPopup("There are no Actions to be removed");
                }
            }, buttonSize, primaryStage);
            Button buttonAddPassage = createButton("Add "+ pageNumberMap.get(pageNumber+1), click -> CreateStory.setScene(pageNumber+1, primaryStage), buttonSize, primaryStage);
            layoutRight.getChildren().addAll(labelEmpty,label, listViewActions, buttonRemoveAction, buttonAddPassage);
            layoutRight.setAlignment(Pos.CENTER);
        }
        /*
          If it is on the
          Create-A-Action scene. Will have buttons
          to select what kind of
          Action you want to make.
         */
        else if(pageNumber == 4) {
            Button buttonHealth = createButton("Health Action", actionEvent -> {healthAction = true;goldAction = false;scoreAction = false;inventoryAction = false;CreateStory.setScene(pageNumber, primaryStage);}, buttonSize-5, primaryStage);
            Button buttonGold = createButton("Gold Action", actionEvent -> {healthAction = false;goldAction = true;scoreAction = false;inventoryAction = false;CreateStory.setScene(pageNumber, primaryStage);}, buttonSize-5, primaryStage);
            Button buttonScore = createButton("Score Action", actionEvent -> {healthAction = false;goldAction = false;scoreAction = true;inventoryAction = false;CreateStory.setScene(pageNumber, primaryStage);}, buttonSize-5, primaryStage);
            Button buttonInventory = createButton("Inventory Action", actionEvent -> {healthAction = false;goldAction = false;scoreAction = false;inventoryAction = true;CreateStory.setScene(pageNumber, primaryStage);}, buttonSize-5, primaryStage);

            VBox actionLayout = new VBox(10);
            actionLayout.getChildren().addAll(new Label(""), new Label(""),buttonHealth, buttonGold, buttonScore, buttonInventory);
            actionLayout.setAlignment(Pos.CENTER);
            layoutRight.getChildren().add(actionLayout);
        }
        return layoutRight;
    }

    /**
     * Method for getting the Pane that goes on
     * the bottom part of the current Scene.
     *
     * @param pageNumber the int value for the page you are on.
     * @return BorderPane.
     */
    private static Pane getBottomPane(int pageNumber){
        BorderPane layoutBottom1 = new BorderPane();

        // Button saving
        Button buttonSave = createButton("Save", click -> {
            if(pageNumber == 1){
                if(titleStory.getText().isEmpty()) {
                    WarningPopup.showHelpPopup("You must enter a title for the Story!");
                }
                if(passageList.isEmpty()) {
                    WarningPopup.showHelpPopup("Your Story must at least have one Passage (Opening Passage)!");
                }
                else {
                    story = new Story(titleStory.getText(), passageList.get(0));
                    for(Passage aPassage : passageList) {
                        if(!story.getPassages().contains(aPassage)){
                            story.addPassage(aPassage);
                        }
                    }
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setInitialFileName(story.getTitle() + ".paths");
                    File file = fileChooser.showSaveDialog(primaryStage);
                    if(file != null) {
                        try {
                            FileHandling.writeStoryToFile(story, file.getParentFile().getPath() + "/");
                            passageList.clear();
                            titleStory.clear();
                            InfoScene.setScene(primaryStage, story, file.getPath());
                        } catch (IOException e) {
                            ErrorPopup.showErrorPopup(e.getMessage(), e.getClass().getSimpleName());
                        }
                    }
                }
            } else if(pageNumber == 2) {
                if(titlePassage.getText().isEmpty()) {
                    WarningPopup.showHelpPopup("You must enter a title for your Passage!");
                } else if (contentPassage.getText().isEmpty()) {
                    passage = new Passage(titlePassage.getText(), "");
                } else {
                    passage = new Passage(titlePassage.getText(), contentPassage.getText());
                }
                if(!linkList.isEmpty()){
                    for(Link aLink : linkList){
                        passage.addLink(aLink);
                    }
                }
                passageList.add(passage);
                linkList.clear();
                titlePassage.clear();
                contentPassage.clear();
                CreateStory.setScene(1, primaryStage);
            } else if(pageNumber == 3) {
                if(referenceLink.getText().isEmpty()) {
                    WarningPopup.showHelpPopup("You must enter a reference for your Link!");
                } else if (textLink.getText().isEmpty()) {
                    link = new Link("", referenceLink.getText());
                } else {
                    link = new Link(textLink.getText(), referenceLink.getText());
                }
                if(!actionList.isEmpty()){
                    for(Action anAction : actionList) {
                        link.addAction(anAction);
                    }
                }
                linkList.add(link);
                actionList.clear();
                referenceLink.clear();
                textLink.clear();
                CreateStory.setScene(2, primaryStage);
            } else if(pageNumber == 4) {
                if(actionAmount.getText().isEmpty()){
                    WarningPopup.showHelpPopup("You must enter a value for your Action!");
                } else {
                    if(healthAction) {
                        action = new HealthAction(Integer.parseInt(actionAmount.getText()));
                    } else if(goldAction) {
                        action = new GoldAction(Integer.parseInt(actionAmount.getText()));
                    } else if(scoreAction) {
                        action = new ScoreAction(Integer.parseInt(actionAmount.getText()));
                    } else if(inventoryAction) {
                        action = new InventoryAction(actionAmount.getText());
                    }
                    actionList.add(action);
                    actionAmount.clear();
                    CreateStory.setScene(3, primaryStage);
                }
            }
        }, buttonSize, primaryStage);
        if(pageNumber != 1) {
            buttonSave.setText("Add "+ pageNumberMap.get(pageNumber)+" to "+ pageNumberMap.get(pageNumber-1)+"?");
        }
        layoutBottom1.setCenter(buttonSave);
        return layoutBottom1;
    }
}
