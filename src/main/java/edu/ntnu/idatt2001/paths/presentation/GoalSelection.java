package edu.ntnu.idatt2001.paths.presentation;

import edu.ntnu.idatt2001.paths.presentation.popup.ErrorPopup;
import edu.ntnu.idatt2001.paths.presentation.popup.HelpPopup;
import edu.ntnu.idatt2001.paths.presentation.popup.WarningPopup;
import edu.ntnu.idatt2001.paths.businessLogic.goals.*;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import edu.ntnu.idatt2001.paths.businessLogic.Game;

import java.util.ArrayList;
import java.util.List;

import static edu.ntnu.idatt2001.paths.presentation.CreateNode.*;

/**
 * Class for selecting and adding Goal´s
 * for the Game.
 *
 * @author Andreas Kluge Svendsrud
 */
public class GoalSelection {
    // List of Goals you want to have for your Game
    private static List<Goal> goals;
    // The primary Stage
    private static Stage primaryStage;
    /**
     * Boolean that is true if you click
     * to create a HealthGoal in the GUI.
     */
    private static boolean healthGoal = false;
    /**
     * Boolean that is true if you click
     * to create a ScoreGoal in the GUI.
     */
    private static boolean scoreGoal = false;
    /**
     * Boolean that is true if you click
     * to create a GoldGoal in the GUI.
     */
    private static boolean goldGoal = false;
    // Variables for creating a InventoryGoal
    private static ListView<String> listViewItems;
    private static final List<String> inventoryGoalList = new ArrayList<>();

    /**
     * Integer that decides the size
     * of Buttons on the Scene. The lower, the bigger
     * the Buttons are.
     */
    private static final int buttonSize = 40;

    /**
     * Integer that decides the size
     * of Texts on the Scene. The lower, the bigger
     * the Texts are.
     */
    private static final int textSize = 40;

    /**
     * Integer that decides the size
     * of Labels on the Scene. The lower, the bigger
     * the Labels are.
     */
    private final static int labelSize = 40;

    /**
     * Integer that decides the size
     * of ListViews on the Scene. The lower, the bigger
     * the ListViews are.
     */
    private static final int listviewSize = 110;

    /**
     * Integer that decides the size
     * of TextFields on the Scene. The lower, the bigger
     * the TextFields are.
     */
    private static final int textfieldSize = 70;

    /**
     * Method for setting scene.
     *
     * @param stage the Stage you want to set a scene for.
     */
    public static void setScene(Stage stage) {
        primaryStage = stage;
        if (goals == null) {
            goals = new ArrayList<>();
        }

        BorderPane borderPane = new BorderPane();
        borderPane.setTop(getTopPane());
        borderPane.setCenter(getCenterPane());
        borderPane.setLeft(getLeftPane());
        borderPane.setRight(getRightPane());
        borderPane.setBottom(getBottomPane());

        Scene scene = new Scene(borderPane, primaryStage.getScene().getWidth(), primaryStage.getScene().getHeight(), Color.DODGERBLUE);
        primaryStage.setScene(scene);
    }

    /**
     * Method for getting the Pane that goes on
     * the top part of the Scene.
     *
     * @return BorderPane.
     */
    private static Pane getTopPane(){
        BorderPane layoutTop = new BorderPane();

        // Text title
        Text textTitle = CreateNode.createText("Goal Selection", textSize, primaryStage);
        textTitle.setTextAlignment(TextAlignment.CENTER);

        // Button for going back and Help
        Button buttonBack = createButton("Go back", click -> PlayerCreation.setScene(primaryStage), buttonSize, primaryStage);
        Button buttonHelp = createButton("Help", click -> HelpPopup.showHelpPopup("goalSelection"),buttonSize, primaryStage);

        layoutTop.setLeft(buttonBack);
        layoutTop.setRight(buttonHelp);
        layoutTop.setCenter(textTitle);
        return layoutTop;
    }

    /**
     * Method for getting the Pane that goes on
     * the center part of the Scene.
     *
     * @return VBox.
     */
    private static Pane getCenterPane() {
        VBox layoutCenter = new VBox(30);
        TextField goalValue = null;
        int intAction = 0;
        if (healthGoal) {
            goalValue = createTextField("<Enter health amount>", intAction, textfieldSize, primaryStage);
        } else if (goldGoal) {
            goalValue = createTextField("<Enter gold amount>", intAction, textfieldSize, primaryStage);
        } else if (scoreGoal) {
            goalValue = createTextField("<Enter score amount>", intAction, textfieldSize, primaryStage);
        }
        TextField finalGoalValue = goalValue;
        Button buttonAdd = CreateNode.createButton("Add Goal", actionEvent -> {
            Goal goal = null;
            if(finalGoalValue == null){
                WarningPopup.showHelpPopup("You must enter a value for your Goal!");
            } else {
                try {
                    if (healthGoal) {
                        goal = new HealthGoal(Integer.parseInt(finalGoalValue.getText()));
                    } else if (goldGoal) {
                        goal = new GoldGoal(Integer.parseInt(finalGoalValue.getText()));
                    } else if (scoreGoal) {
                        goal = new ScoreGoal(Integer.parseInt(finalGoalValue.getText()));
                    }
                    goals.add(goal);
                    setScene(primaryStage);
                } catch(NumberFormatException ex) {
                    ErrorPopup.showErrorPopup("You must enter in a number for this Goal!", ex.getClass().getSimpleName());
                } catch(IllegalArgumentException ex) {
                    ErrorPopup.showErrorPopup(ex.getMessage(), ex.getClass().getSimpleName());
                }
            }}, buttonSize + 10, primaryStage);
        if(healthGoal | goldGoal | scoreGoal) {
            layoutCenter.getChildren().addAll(goalValue, buttonAdd);
        } else layoutCenter.getChildren().remove(buttonAdd);
        layoutCenter.setAlignment(Pos.CENTER);
        return layoutCenter;
    }

    /**
     * Method for getting the Pane that goes on
     * the left part of the Scene.
     *
     * @return VBox.
     */
    private static Pane getLeftPane() {
        VBox actionLayout = new VBox(15);

        Button buttonHealth = createButton("Health Goal", actionEvent -> {
            healthGoal = true;goldGoal = false;scoreGoal = false;
            setScene(primaryStage);}, buttonSize, primaryStage);
        Button buttonGold = createButton("Gold Goal", actionEvent -> {
            healthGoal = false;goldGoal = true;scoreGoal = false;
            setScene(primaryStage);}, buttonSize, primaryStage);
        Button buttonScore = createButton("Score Goal", actionEvent -> {
            healthGoal = false;goldGoal = false;scoreGoal = true;
            setScene(primaryStage);}, buttonSize, primaryStage);
        Button buttonInventory = createButton("Inventory Goal", actionEvent -> {
            healthGoal = false;goldGoal = false;scoreGoal = false;
            primaryStage.setScene(getInventoryScene());
            }, buttonSize, primaryStage);

        actionLayout.getChildren().addAll(buttonHealth, buttonGold, buttonScore, buttonInventory);
        actionLayout.setAlignment(Pos.CENTER_LEFT);

        return actionLayout;
    }

    /**
     * Method for getting the Pane that goes on
     * the bottom part of the Scene.
     *
     * @return VBox.
     */
    private static Pane getBottomPane() {
        HBox bottomLayout = new HBox();
        Button buttonSave = CreateNode.createButton("Start", actionEvent -> DisplayPassage.setScene(primaryStage, new Game(PlayerCreation.getPlayer(), InfoScene.getStory(), getGoals())), buttonSize, primaryStage);
        bottomLayout.getChildren().add(buttonSave);
        bottomLayout.setAlignment(Pos.CENTER);
        return bottomLayout;
    }


    /**
     * Method for getting the
     * Scene for creating
     * InventoryGoals
     *
     * @return Scene
     */
    private static Scene getInventoryScene() {
        BorderPane inventoryPane = new BorderPane();
        Scene inventoryScene = new Scene(inventoryPane, primaryStage.getScene().getWidth(), (primaryStage.getScene().getHeight()));

        BorderPane layoutTop = new BorderPane();

        // Top layout
        Text textTitle = CreateNode.createText("Create InventoryGoal", textSize, primaryStage);
        textTitle.setTextAlignment(TextAlignment.CENTER);
        Button buttonBack = createButton("Go back", click -> setScene(primaryStage), buttonSize, primaryStage);
        Button buttonHelp = createButton("Help", click -> HelpPopup.showHelpPopup("inventoryGoal"),buttonSize, primaryStage);
        layoutTop.setLeft(buttonBack);
        layoutTop.setRight(buttonHelp);
        layoutTop.setCenter(textTitle);

        // Right layout
        VBox listLayout = new VBox(10);
        Label labelEmpty = createLabel("", labelSize, primaryStage);
        Label label = createLabel("List of Items:", labelSize, primaryStage);
        listViewItems = createListView(inventoryGoalList, "Items", listviewSize, primaryStage);
        Button buttonRemoveItem = CreateNode.createButton("Remove Item", actionEvent -> {
            try {
                int selectedIndex = listViewItems.getSelectionModel().getSelectedIndex();
                if (selectedIndex != -1) {
                    inventoryGoalList.remove(selectedIndex);
                    primaryStage.setScene(getInventoryScene());
                } else {
                    WarningPopup.showHelpPopup("You must select an Item!");
                }
            } catch(IndexOutOfBoundsException ex) {
                ErrorPopup.showErrorPopup("You must add an Item, to be able to remove.", "No Items in List");
            }

        }, buttonSize, primaryStage);
        listLayout.getChildren().addAll(labelEmpty,label, listViewItems, buttonRemoveItem);

        // Center layout
        String stringAction = "";
        TextField itemTextField = createTextField("<Enter item>", stringAction, textfieldSize+20, primaryStage);
        Button buttonAdd = CreateNode.createButton("Add Item", actionEvent2 -> {
            if (itemTextField.getText().isBlank()) {
                WarningPopup.showHelpPopup("You must enter a value for your InventoryGoal!");
            } else {
                inventoryGoalList.add(itemTextField.getText());
                primaryStage.setScene(getInventoryScene());
                }
            }, buttonSize + 10, primaryStage);
        VBox centerLayout = new VBox(10);
        centerLayout.getChildren().addAll(itemTextField, buttonAdd);
        centerLayout.setAlignment(Pos.CENTER);

        // Bottom layout
        Button buttonInventoryGoal = CreateNode.createButton("Add InventoryGoal", actionEvent2 -> {
            List<String> inventoryGoalListCopy = new ArrayList<>(inventoryGoalList);
            goals.add(new InventoryGoal(inventoryGoalListCopy));
            setScene(primaryStage);
            inventoryGoalList.clear();
        }, buttonSize, primaryStage);
        HBox bottomLayout = new HBox(20);
        if(!inventoryGoalList.isEmpty()) {
            bottomLayout.getChildren().add(buttonInventoryGoal);
            bottomLayout.setAlignment(Pos.CENTER);
        }

        inventoryPane.setTop(layoutTop);
        inventoryPane.setCenter(centerLayout);
        inventoryPane.setRight(listLayout);
        inventoryPane.setBottom(bottomLayout);
        return inventoryScene;
    }

    /**
     * Method for getting the Pane that goes on
     * the right part of the Scene.
     *
     * @return VBox.
     */
    private static Pane getRightPane() {
        VBox rightPane = new VBox(15);

        // List of Links in the Story
        Label labelEmpty = createLabel("", labelSize, primaryStage);
        Label label = createLabel("List of Goals:", labelSize, primaryStage);
        listViewItems = createListView(goals, "Goals", listviewSize, primaryStage);
        Button buttonRemoveGoal = CreateNode.createButton("Remove Goal", actionEvent -> {
            try {
                int selectedIndex = listViewItems.getSelectionModel().getSelectedIndex();
                if(selectedIndex != -1) {
                        getGoals().remove(selectedIndex);
                    setScene(primaryStage);
                } else {
                    WarningPopup.showHelpPopup("You must select an Item!");
                }
            } catch(IndexOutOfBoundsException ex) {
                WarningPopup.showHelpPopup("There are no Goals to be removed");
            }

        }, buttonSize, primaryStage);

        rightPane.getChildren().addAll(labelEmpty,label, listViewItems, buttonRemoveGoal);

        return rightPane;
    }
    private static List<Goal> getGoals() {
        return GoalSelection.goals;
    }
}
