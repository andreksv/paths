package edu.ntnu.idatt2001.paths.presentation;

import edu.ntnu.idatt2001.paths.businessLogic.Story;
import edu.ntnu.idatt2001.paths.data.FileFormatException;
import edu.ntnu.idatt2001.paths.data.FileHandling;
import edu.ntnu.idatt2001.paths.data.InvalidFileException;
import edu.ntnu.idatt2001.paths.presentation.popup.ErrorPopup;
import edu.ntnu.idatt2001.paths.presentation.popup.HelpPopup;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static edu.ntnu.idatt2001.paths.presentation.CreateNode.createButton;

/**
 * Method for getting the Scene
 * for picking a Story. Also, possible
 * to upload Stories.
 *
 * @author Andreas Kluge Svendsrud
 */
public class PickStory {
    // The primary Stage
    private static Stage primaryStage;
    /**
     * Map of different Stories, with Integers
     * from 1, 2, 3 etc. as Keys.
     */
    private static Map<Integer, Story> storyMap;
    /**
     * Map of FilePath to different Stories,
     * with Integers from 1, 2, 3 etc. as Keys.
     */
    private static Map<Integer, String> filePathMap;

    /**
     * Integer that decides the size
     * of Buttons on the Scene. The lower, the bigger
     * the Buttons are.
     */
    static int buttonSize = 40;

    /**
     * Integer that decides the size
     * of Texts on the Scene. The lower, the bigger
     * the Texts are.
     */
    static int textSize = 30;

    /**
     * Method for setting scene.
     *
     * @param stage the window you want to set a scene for.
     */

    public static void setScene(Stage stage) {
        if(storyMap == null) {
            storyMap = new HashMap<>();
        }
        if(filePathMap == null) {
            filePathMap = new HashMap<>();
        }
        primaryStage = stage;
        BorderPane borderPane = new BorderPane();

        borderPane.setRight(getRightPane());
        borderPane.setLeft(getLeftPane());

        Scene scene = new Scene(borderPane, primaryStage.getScene().getWidth(), primaryStage.getScene().getHeight(), Color.DODGERBLUE);
        primaryStage.setScene(scene);
    }

    /**
     * Method for getting the Pane that goes on
     * the left part of the Scene.
     *
     * @return HBox.
     */
    private static Pane getLeftPane() {
        HBox leftPaneHorizontal = new HBox(50);
        VBox leftPaneVertical = new VBox(40);

        Text title = CreateNode.createText("Pick a Story", textSize, primaryStage);
        Button help = CreateNode.createButton("Help", actionEvent -> HelpPopup.showHelpPopup("pickStory"), buttonSize, primaryStage);
        Button uploadStory = createButton("Upload story", click -> {
            FileChooser fileChooser = new FileChooser();
            File file = fileChooser.showOpenDialog(primaryStage);
            if(file != null) {
                try {
                    Story story = FileHandling.readStoryFromFile(file.getPath());

                    storyMap.put(storyMap.size(), story);
                    filePathMap.put(filePathMap.size(), file.getPath());

                    PickStory.setScene(primaryStage);
                } catch (FileFormatException | InvalidFileException | IOException e) {
                    ErrorPopup.showErrorPopup(e.getMessage(), e.getClass().getSimpleName());
                }
            }
        }, buttonSize, primaryStage);
        Button goBack = CreateNode.createButton("Go Back", actionEvent -> MainMenu.getMainMenuScene(primaryStage), buttonSize, primaryStage);

        leftPaneVertical.getChildren().addAll(new Label(""), title, help, uploadStory, goBack);
        leftPaneVertical.setAlignment(Pos.TOP_CENTER);
        leftPaneHorizontal.getChildren().addAll(new Label(""), leftPaneVertical);

        return leftPaneHorizontal;
    }

    /**
     * Method for getting the Pane that goes on
     * the right part of the Scene.
     *
     * @return HBox.
     */
    private static Pane getRightPane() {
        HBox rightPaneHorizontal = new HBox(30);
        VBox rightPaneVertical = new VBox(30);
        rightPaneVertical.getChildren().add(new Label(""));
        if(!storyMap.isEmpty()) {
            rightPaneVertical.getChildren().addAll(CreateNode.createText("Choose a Story: ", textSize+10, primaryStage));
            for (int i = 0; i <= storyMap.size(); i ++) {
                if(storyMap.containsKey(i)) {
                    int finalI = i;
                    rightPaneVertical.getChildren().add(CreateNode.createButton(storyMap.get(i).getTitle(), actionEvent -> InfoScene.setScene(primaryStage, storyMap.get(finalI), filePathMap.get(finalI)), buttonSize, primaryStage));
                }
            }
        } else {
            rightPaneVertical.getChildren().addAll(CreateNode.createText("NO STORIES AVAILABLE", textSize+10, primaryStage));
        }
        rightPaneVertical.setAlignment(Pos.CENTER);
        rightPaneHorizontal.getChildren().addAll(rightPaneVertical, new Label(""));
        return rightPaneHorizontal;
    }


}
