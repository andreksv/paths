module edu.ntnu.idatt2001.paths.presentation.MainMenu {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;

    exports edu.ntnu.idatt2001.paths.presentation;
    opens edu.ntnu.idatt2001.paths.presentation to javafx.fxml;
    exports edu.ntnu.idatt2001.paths.presentation.popup;
    opens edu.ntnu.idatt2001.paths.presentation.popup to javafx.fxml;
    exports edu.ntnu.idatt2001.paths.businessLogic;
    opens edu.ntnu.idatt2001.paths.businessLogic to javafx.fxml;
}
