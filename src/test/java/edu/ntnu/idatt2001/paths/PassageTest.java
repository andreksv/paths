package edu.ntnu.idatt2001.paths;

import edu.ntnu.idatt2001.paths.businessLogic.Link;
import edu.ntnu.idatt2001.paths.businessLogic.Passage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;


class PassageTest {
    @Test
    @DisplayName("Check that the constructor is initialized")
    void CheckTheConstructorIsInitialized(){
        Passage passage = new Passage("Forest", "It raining in the forest");
        String textPassage = passage.getTitle();
        String forest = "Forest";
        assertEquals(forest,textPassage);
        assertEquals("It raining in the forest",passage.getContent());
        assertNotNull(passage);
    }
    @Nested
    @DisplayName("Test addLinkToPassage method")
    class addLinkToPassage{
        Passage passage = new Passage("Forest", "It raining in the forest");

        @Test
        @DisplayName("addLink returns true when adding different Link´s")
        void addNewLinkToPassage(){
            Link newLink1 = new Link("You come upon two different paths...", "The choice of paths");
            Link newLink2 = new Link("Before you go out, should you bring an Umbrella?", "Umbrella?");
            assertTrue(passage.addLink(newLink1));
            assertTrue(passage.addLink(newLink2));
        }

        @Test
        @DisplayName("addLink returns false when trying to add the same Link")
        void addSameLinkToPassage(){
            Link newLink = new Link("You come upon two different paths...", "The choice of paths");
            passage.addLink(newLink);
            assertFalse(passage.addLink(newLink));
        }
    }

    @Nested
    @DisplayName("Testing all getMethods")
    class getMethods {
        Passage passage = new Passage("Forest", "It raining in the forest");

        @Test
        @DisplayName("getTitle gives correct title")
        void getTitleTest() {
            assertEquals("Forest", passage.getTitle());
        }

        @Test
        @DisplayName("getContent gives correct content")
        void getContentTest() {
            assertEquals("It raining in the forest", passage.getContent());
        }

        @Test
        @DisplayName("getLinks gives correct Link objects back")
        void getLinksTest() {
            Link newLink1 = new Link("You come upon two different paths...", "The choice of paths");
            Link newLink2 = new Link("Before you go out, should you bring an Umbrella?", "Umbrella?");
            passage.addLink(newLink1);
            passage.addLink(newLink2);
            List<Link> links = new ArrayList<>();
            links.add(newLink1);
            links.add(newLink2);
            assertTrue(passage.getLinks().contains(new Link("You come upon two different paths...", "The choice of paths")) && passage.getLinks().contains(new Link("Before you go out, should you bring an Umbrella?", "Umbrella?")));
            assertEquals(links, passage.getLinks());
        }
    }

    @Nested
    @DisplayName("Testing hasLinks method")
    class checkIfPassageHasLinks{
        Passage passage = new Passage("Forest", "It raining in the forest");
        @Test
        @DisplayName("Checking if hasLinks returns false if passage has no Link´s")
        void passageHasNoLinks(){
            assertFalse(passage.hasLinks());
        }

        @Test
        @DisplayName("Checking if hasLinks returns true if passage has Link´s")
        void passageHasLinks(){
            Link newLink1 = new Link("You come upon two different paths...", "The choice of paths");
            Link newLink2 = new Link("Before you go out, should you bring an Umbrella?", "Umbrella?");
            passage.addLink(newLink1);
            passage.addLink(newLink2);
            assertTrue(passage.hasLinks());

        }
    }

    @Nested
    @DisplayName("Testing equals method")
    class comparePassage{
        Passage passage = new Passage("Forest", "It raining in the forest");
        @Test
        @DisplayName("Comparing the same passage")
        void samePassage(){
            Passage samePassage = new Passage("Forest", "It raining in the forest");
            assertEquals(passage, samePassage);
        }
        @Test
        @DisplayName("Comparing different passages")
        void differentPassage(){
            Passage differentPassage = new Passage("Desert", "Its burning in the desert");
            assertNotEquals(passage, differentPassage);
        }
    }
}