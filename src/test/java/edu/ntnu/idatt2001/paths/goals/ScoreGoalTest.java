package edu.ntnu.idatt2001.paths.goals;

import edu.ntnu.idatt2001.paths.businessLogic.Player;
import edu.ntnu.idatt2001.paths.businessLogic.goals.ScoreGoal;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ScoreGoalTest{
    int minimumScore = 527;
    ScoreGoal scoreGoalTest = new ScoreGoal(minimumScore);
    @Nested
    @DisplayName("Check if player has fulfilled score goal")
    class checkIfAPlayerHasFulfilledGoal{
        @Test
        @DisplayName("isFulfilled returns false when goal is not fulfilled")
        public void playerHasNotFulfilledGoal(){
            Player player = new Player.Builder()
                    .score(500)
                    .build();
            assertFalse(scoreGoalTest.isFulfilled(player));
        }
        @Test
        @DisplayName("isFulfilled returns true when goal is fulfilled")
        public void playerHasFulfilledGoal(){
            Player player = new Player.Builder()
                    .score(750)
                    .build();
            assertTrue(scoreGoalTest.isFulfilled(player));
        }
    }
}