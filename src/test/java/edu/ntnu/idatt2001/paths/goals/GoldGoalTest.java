package edu.ntnu.idatt2001.paths.goals;

import edu.ntnu.idatt2001.paths.businessLogic.Player;
import edu.ntnu.idatt2001.paths.businessLogic.goals.GoldGoal;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GoldGoalTest {
    int minimumGold = 1000;
    GoldGoal goldGoal = new GoldGoal(minimumGold);
    @Nested
    @DisplayName("Check if player has fulfilled gold goal")
    class checkIfAPlayerHasFulfilledGoal{
        @Test
        @DisplayName("isFulfilled returns false when goal is not fulfilled")
        public void playerHasNotFulfilledGoal(){
            Player player = new Player.Builder()
                    .gold(999).build();
            assertFalse(goldGoal.isFulfilled(player));
        }
        @Test
        @DisplayName("isFulfilled returns true when goal is fulfilled")
        public void playerHasFulfilledGoal(){
            Player player = new Player.Builder()
                    .gold(2500)
                    .build();
            assertTrue(goldGoal.isFulfilled(player));
        }


    }
}
