package edu.ntnu.idatt2001.paths.goals;

import edu.ntnu.idatt2001.paths.businessLogic.Player;
import edu.ntnu.idatt2001.paths.businessLogic.goals.HealthGoal;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class HealthGoalTest {
    int minimumHealthTest = 100;

    HealthGoal healthGoalTest = new HealthGoal(minimumHealthTest);

    @Nested
    @DisplayName("Check if player has fulfilled health goal")
    class checkIfAPlayerHasFulfilledGoal{
        @Test
        @DisplayName("isFulfilled returns false when goal is not fulfilled")
        public void playerHasNotFulfilledGoal(){
            Player player = new Player.Builder()
                    .health(75)
                    .build();
            assertFalse(healthGoalTest.isFulfilled(player));
        }

        @Test
        @DisplayName("isFulfilled returns true when goal is fulfilled")
        public void playerHasFulfilledGoal(){
            Player player = new Player.Builder()
                    .health(150)
                    .build();
            assertTrue(healthGoalTest.isFulfilled(player));
        }
    }
}
