package edu.ntnu.idatt2001.paths.goals;

import edu.ntnu.idatt2001.paths.businessLogic.Player;
import java.util.List;
import java.util.ArrayList;

import edu.ntnu.idatt2001.paths.businessLogic.goals.InventoryGoal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class InventoryGoalTest {
    @Nested
    @DisplayName("Check if player has fulfilled inventory goal")
    class checkIfAPlayerHasFulfilledGoal {
        InventoryGoal inventoryGoalTest;
        @BeforeEach
        public void createInventoryGoalListWithTestItems() {
            List<String> mandatoryItems = new ArrayList<>();

            mandatoryItems.add("Sword");
            mandatoryItems.add("Health Potion");
            mandatoryItems.add("Math Book");
            inventoryGoalTest = new InventoryGoal(mandatoryItems);
        }

        @Test
        @DisplayName("isFulfilled returns false when Player has none of the items needed")
        public void playerHasNoneOfTheItems() {
            Player player = new Player.Builder().build();
            player.addToInventory("Bike");
            player.addToInventory("Club");
            player.addToInventory("Disco Ball");
            assertFalse(inventoryGoalTest.isFulfilled(player));
        }

        @Test
        @DisplayName("isFulfilled returns true when Player has all items needed")
        public void playerHasAllTheItems(){
            Player player = new Player.Builder().build();
            player.addToInventory("Sword");
            player.addToInventory("Health Potion");
            player.addToInventory("Math Book");
            assertTrue(inventoryGoalTest.isFulfilled(player));
        }

        @Test
        @DisplayName("isFulfilled returns false when Player has some (not all) of items needed")
        public void playerHasTwoOfTheItems(){
            Player player = new Player.Builder().build();
            player.addToInventory("Sword");
            player.addToInventory("Health Potion");
            assertFalse(inventoryGoalTest.isFulfilled(player));
            }
        }
}
