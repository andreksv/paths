package edu.ntnu.idatt2001.paths.ActionTests;

import edu.ntnu.idatt2001.paths.businessLogic.actions.GoldAction;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import edu.ntnu.idatt2001.paths.businessLogic.Player;

import static junit.framework.Assert.assertTrue;

public class GoldActionTest {
    @Nested
    @DisplayName("Test execute method for a GoldAction")
    class testExecuteGoldAction {
        @Test
        @DisplayName("Execute can give more gold to a Player")
        public void CheckIfGoldActionIncreasesGoldAmount() {
            Player player = new Player.Builder()
                    .gold(50)
                    .build();
            GoldAction goldAction = new GoldAction(25);
            goldAction.execute(player);
            assertTrue(player.getGold() > 50);

        }
        @Test
        @DisplayName("Execute can give less gold to a Player")
        public void CheckIfGoldActionDecreasesGoldAmount() {
            Player rian = new Player.Builder()
                    .gold(100)
                    .build();
            GoldAction decreaseGoldAction = new GoldAction(-45);
            decreaseGoldAction.execute(rian);
            assertTrue(rian.getGold() < 100);
        }
    }
}
