package edu.ntnu.idatt2001.paths.ActionTests;

import edu.ntnu.idatt2001.paths.businessLogic.actions.InventoryAction;
import edu.ntnu.idatt2001.paths.businessLogic.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class InventoryActionTest {
    @Nested
    @DisplayName("Test execute-method for an InventoryAction")
    class testExecuteInventoryAction {
        @Test
        @DisplayName("Execute can add an item to a Player inventory")
        public void InventoryActionTestAddAItemToTheInventory() {
            Player itemPlayer = new Player.Builder()
                    .build();
            InventoryAction addItem = new InventoryAction("Treebranch");
            addItem.execute(itemPlayer);
            assertNotNull(itemPlayer.getInventory());
        }

        @Test
        @DisplayName("Constructor throws NullPointerException when it should")
        public void ExceptionWhenItemIsNull() {
            assertThrows(NullPointerException.class, () -> {
                new InventoryAction(null);
            });
        }
    }
}
