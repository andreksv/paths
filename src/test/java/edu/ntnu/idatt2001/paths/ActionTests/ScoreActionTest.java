package edu.ntnu.idatt2001.paths.ActionTests;

import edu.ntnu.idatt2001.paths.businessLogic.actions.ScoreAction;
import edu.ntnu.idatt2001.paths.businessLogic.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static junit.framework.Assert.assertTrue;

public class ScoreActionTest {
    @Nested
    @DisplayName("Test execute method for a ScoreAction")
    class testExecuteScoreAction {
        @Test
        @DisplayName("Execute can add to the score of a Player")
        public void ScoreActionIncreaseTheScore() {
            Player playerIncrease = new Player.Builder().build();
            ScoreAction spell = new ScoreAction(50);
            spell.execute(playerIncrease);
            assertTrue(playerIncrease.getScore() > 0);
        }

        @Test
        @DisplayName("Execute can reduce the score of a Player")
        public void ScoreActionDecreaseTheScore() {
            Player playerDecrease = new Player.Builder()
                    .score(100)
                    .build();
            ScoreAction spell = new ScoreAction(-50);
            spell.execute(playerDecrease);
            assertTrue(playerDecrease.getScore() < 100);

        }
    }
}
