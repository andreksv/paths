package edu.ntnu.idatt2001.paths.ActionTests;

import edu.ntnu.idatt2001.paths.businessLogic.actions.HealthAction;
import edu.ntnu.idatt2001.paths.businessLogic.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static junit.framework.Assert.assertTrue;

public class HealthActionTest {
    @Nested
    @DisplayName("Test execute method for a HealthAction")
    class testExecuteHealthAction {
        @Test
        @DisplayName("Execute can give more health to a Player")
        public void CheckIfHealthOfPlayerIncreases() {
            Player playerIncrease = new Player.Builder()
                    .health(20)
                    .build();
            HealthAction spell = new HealthAction(50);
            spell.execute(playerIncrease);
            assertTrue(playerIncrease.getHealth() > 20);
        }

        @Test
        @DisplayName("Execute can give less health to a Player")
        public void CheckIfHealthOfPlayerDecreases() {
            Player playerDecreases = new Player.Builder()
                    .build();
            HealthAction run = new HealthAction(-50);
            run.execute(playerDecreases);
            assertTrue(playerDecreases.getHealth() < 100);
        }
    }
}
