package edu.ntnu.idatt2001.paths;

import edu.ntnu.idatt2001.paths.businessLogic.actions.GoldAction;
import edu.ntnu.idatt2001.paths.businessLogic.actions.InventoryAction;
import edu.ntnu.idatt2001.paths.businessLogic.Link;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import edu.ntnu.idatt2001.paths.businessLogic.actions.Action;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
public class LinkTest {
    @Nested
    @DisplayName("Test getMethods")
    class getMethods{
        Link linkTest;
        @BeforeEach
        void createLinkWithActions() {
            linkTest = new Link("A delicious cake stands before you. What do you do?", "A chocolate cake");
            Action goldActionTest = new GoldAction(50);
            Action inventoryActionTest = new InventoryAction("Sword");
            linkTest.addAction(goldActionTest);
            linkTest.addAction(inventoryActionTest);
        }
        @Test
        @DisplayName("Check if getText gives correct value")
        void getTextTest() {
            String linkText = "A delicious cake stands before you. What do you do?";
            assertEquals(linkText, linkTest.getText());
        }

        @Test
        @DisplayName("Check if getReference gives correct value")
        void getReferenceTest(){
            String linkReference = "A chocolate cake";
            assertEquals(linkReference, linkTest.getReference());
        }

        @Test
        @DisplayName("Check if getAction gives all action")
        void getActionsTest(){
            List<Action> actionsCompareLinkTest = new ArrayList<>(linkTest.getActions());
            actionsCompareLinkTest.forEach((action -> assertTrue(linkTest.getActions().contains(action))));
        }
    }

    @Nested
    @DisplayName("Test addAction method")
    class addAction {
        Link linkTest;
        @BeforeEach
        void createLinkWithoutActions() {
            linkTest = new Link("A delicious cake stands before you. What do you do?", "A chocolate cake");
        }

        @Test
        @DisplayName("addAction does not throw exception when it should not")
        void addNewActionTest(){
            Action actionGold = new GoldAction(50);
            assertDoesNotThrow(() -> linkTest.addAction(actionGold));
        }

        @Test
        @DisplayName("addAction throws exception when it should")
        void addSameActionTest(){
                Action actionGold = new GoldAction(50);
                linkTest.addAction(actionGold);
                assertThrows(IllegalArgumentException.class, () -> {linkTest.addAction(actionGold); fail("Method did not throw IllegalArgumentException as expected");});
        }
    }

    @Nested
    @DisplayName("Test equals method")
    class compareLinks{
        @Test
        @DisplayName("Comparing two Link objects that are the same")
        void compareSameObject(){
            Link testLink1 = new Link("Someone is knocking at the door, and it is NOT the wind", "The quiet knock");
            Link testLink2 = new Link("Someone is knocking at the door, and it is NOT the wind", "The quiet knock");
            assertEquals(testLink1, testLink2);
        }
        @Test
        @DisplayName("Comparing two different Link objects")
        void compareDifferentObject(){
            Link testLink1 = new Link("Someone is knocking at the door, and it is NOT the wind", "The quiet knock");
            Link testLink2 = new Link("It has spotted you and is running towards you...", "A GIANT monster");
            assertNotEquals(testLink1, testLink2);
        }
    }
}
