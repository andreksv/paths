package edu.ntnu.idatt2001.paths;

import java.util.List;
import java.util.ArrayList;

import edu.ntnu.idatt2001.paths.businessLogic.*;
import edu.ntnu.idatt2001.paths.businessLogic.goals.Goal;

import edu.ntnu.idatt2001.paths.businessLogic.goals.GoldGoal;
import edu.ntnu.idatt2001.paths.businessLogic.goals.HealthGoal;
import edu.ntnu.idatt2001.paths.businessLogic.goals.ScoreGoal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class GameTest {
    Game gameTest;
    Story storyTest;
    @BeforeEach
    public void createGame(){
        Player playerTest = new Player.Builder()
                .name("Max")
                .build();
        Passage openingPassageTest = new Passage("A vivid dream", "");
        storyTest = new Story("The Nightmare", openingPassageTest);
        List<Goal> goals = new ArrayList<>();
        gameTest = new Game(playerTest, storyTest, goals); //Creating an instance of Game class for testing
    }

    @Nested
    @DisplayName("Testing all getMethods")
    class getMethods{
        @Test
        @DisplayName("getPlayer gives correct Player")
        void getPlayerTest(){
            assertEquals(gameTest.getPlayer(), new Player.Builder()
                    .name("Max")
                    .build());
        }

        @Test
        @DisplayName("getStory gives correct Story")
        void getStoryTest(){
            assertEquals(gameTest.getStory(), new Story("The Nightmare", new Passage("A vivid dream", "")));
        }

        @Test
        @DisplayName("getGoals gives correct Goals")
        void getGoals(){
            assertEquals(gameTest.getGoals(), new ArrayList<>());
        }
    }
    @Nested
    @DisplayName("Test go and begin methods")
    class goAndBeginMethods{
        @Test
        @DisplayName("begin-method gives correct passage back")
        void beginTest(){
            assertEquals(gameTest.begin(),new Passage("A vivid dream", ""));
        }
        @Test
        @DisplayName("go-method does not throw exception when it should not")
        void tryToFindAPassageThatExist(){
            Passage testPassage = new Passage("Forest", "It raining in the forest");
            storyTest.addPassage(testPassage);
            Link findTestPassageLink = new Link(testPassage.getTitle(), testPassage.getTitle());
            assertDoesNotThrow(() -> gameTest.go(findTestPassageLink));
        }
    }

    @Test
    @DisplayName("Test getFulfilledGoals gives List with expected Goals back")
    void getFulfilledGoalsGivesListWithExpectedGoalsBack() {
        gameTest.getPlayer().addGold(100);
        gameTest.getPlayer().addHealth(400);
        GoldGoal fulfilledGoldGoal = new GoldGoal(70);
        HealthGoal fulfilledHealthGoal = new HealthGoal(350);
        ScoreGoal notFulfilledScoreGoal = new ScoreGoal(1000);
        gameTest.addGoal(fulfilledGoldGoal);
        gameTest.addGoal(fulfilledHealthGoal);
        gameTest.addGoal(notFulfilledScoreGoal);

        assertTrue(gameTest.getFulfilledGoals().contains(fulfilledGoldGoal));
        assertTrue(gameTest.getFulfilledGoals().contains(fulfilledHealthGoal));
        assertFalse(gameTest.getFulfilledGoals().contains(notFulfilledScoreGoal));
    }
}
