package edu.ntnu.idatt2001.paths.presentation;

import edu.ntnu.idatt2001.paths.presentation.popup.Help;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class HelpTest {
    @Test
    @DisplayName("getValue gives expected value back")
    void getValueGivesExpectedValueBack(){
        assertEquals(Help.getValue("mainMenu"), "Welcome to the game of Paths. This is a game engine for choice-based and interactive story games.\n\n Create the game you want to play by picking from multiple possible Stories, designing your own characters and picking the goals of the game!\n\n If this is your first time playing click on the \"Pick a Story!\" to get started and choosing which story you will play.");
    }

    @Test
    @DisplayName("getValue throws exception when it should")
    void getValueThrowsExceptionWhenItShould(){
        assertThrows(NullPointerException.class, () -> Help.getValue("Not a scene"));
    }
}
