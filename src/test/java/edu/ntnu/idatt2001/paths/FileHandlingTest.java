package edu.ntnu.idatt2001.paths;

import edu.ntnu.idatt2001.paths.businessLogic.actions.GoldAction;
import edu.ntnu.idatt2001.paths.businessLogic.actions.HealthAction;
import edu.ntnu.idatt2001.paths.businessLogic.actions.InventoryAction;
import edu.ntnu.idatt2001.paths.businessLogic.actions.ScoreAction;
import edu.ntnu.idatt2001.paths.data.FileFormatException;
import edu.ntnu.idatt2001.paths.data.FileHandling;
import edu.ntnu.idatt2001.paths.data.InvalidFileException;
import edu.ntnu.idatt2001.paths.businessLogic.Link;
import edu.ntnu.idatt2001.paths.businessLogic.Passage;
import edu.ntnu.idatt2001.paths.businessLogic.Story;
import org.junit.jupiter.api.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class FileHandlingTest {
    Story storyTest;
    @Nested
    @DisplayName("Trying to write and read from a file with a negative value in an Action")
    class readStoryFromFileWithNegativeActionValue{
        @BeforeEach
        void createStoryWithOnlyOpeningPassageLinksActions() {
            Passage passageBeginnings = new Passage("Beginnings", "You are in a small, dimly lit room. There is a door in front of you.");
            Link linkAnotherRoom = new Link("Try to open the door", "Another room");
            HealthAction healthActionAnotherRoom = new HealthAction(-50);
            linkAnotherRoom.addAction(healthActionAnotherRoom);
            passageBeginnings.addLink(linkAnotherRoom);
            storyTest = new Story("fileHandlingTest", passageBeginnings);
        }

        @Test
        @DisplayName("writeStoryToFile does not throw exception when it should not")
        void writeToFileTest() {
            assertDoesNotThrow(() -> FileHandling.writeStoryToFile(storyTest,"src/test/resources/"));
        }

        @Test
        @DisplayName("readStoryFromFile gives correct story back")
        void readFromFileTest() throws IOException, FileFormatException, InvalidFileException {
            assertEquals(FileHandling.readStoryFromFile("src/test/resources/fileHandlingTest.paths"), storyTest);
        }
    }
    @Nested
    @DisplayName("FileHandling a Story with only openingPassage")
    class fileHandlingOnlyOpeningPassage {
        Passage passageBeginnings;
        @BeforeEach
        void createStoryWithOnlyOpeningPassage() {
            passageBeginnings = new Passage("Beginnings", "You are in a small, dimly lit room. There is a door in front of you.");
            storyTest = new Story("fileHandlingTest", passageBeginnings);
        }

        @Nested
        @DisplayName("openingPassage with no content and no links")
        class fileHandlingOnlyOpeningPassageEmptyContent {

            @BeforeEach
            void createStoryWithOnlyOpeningPassageEmptyContent() {
                passageBeginnings = new Passage("Beginnings", "");
                storyTest = new Story("fileHandlingTest", passageBeginnings);
            }
            @Test
            @DisplayName("writeStoryToFile does not throw exception when it should not")
            void writeToFileTest() {
                assertDoesNotThrow(() -> FileHandling.writeStoryToFile(storyTest,"src/test/resources/"));
            }
            @Test
            @DisplayName("readStoryFromFile gives correct story back")
            void readFromFileTest() throws IOException, FileFormatException, InvalidFileException {
                assertEquals(storyTest, FileHandling.readStoryFromFile("src/test/resources/fileHandlingTest.paths"));
            }
        }

        @Nested
        @DisplayName("No Link´s")
        class fileHandlingOnlyOpeningPassageNoLinks {
            @Test
            @DisplayName("writeStoryToFile does not throw exception when it should not")
            void writeToFileTest() {
                assertDoesNotThrow(() -> FileHandling.writeStoryToFile(storyTest,"src/test/resources/"));
            }

            @Test
            @DisplayName("readStoryFromFile gives correct story back")
            void readFromFileTest() throws IOException, FileFormatException, InvalidFileException {
                assertEquals(FileHandling.readStoryFromFile("src/test/resources/fileHandlingTest.paths"), storyTest);
            }
        }

        @Nested
        @DisplayName("openingPassage with no content, but has Link´s")
        class fileHandlingOnlyOpeningPassageLinkEmptyContent {
            @BeforeEach
            void createStoryWithOnlyOpeningPassageLinksNoContent() {
                passageBeginnings = new Passage("Beginnings", "");
                Link linkExit = new Link("You exit the building", "Escaped!");
                Link linkAnotherRoom = new Link("Try to open the door", "Another room");
                passageBeginnings.addLink(linkAnotherRoom);
                passageBeginnings.addLink(linkExit);
                storyTest = new Story("fileHandlingTest", passageBeginnings);
            }
            @Test
            @DisplayName("writeStoryToFile does not throw exception when it should not")
            void writeToFileTest() {
                assertDoesNotThrow(() -> FileHandling.writeStoryToFile(storyTest,"src/test/resources/"));
            }
            @Test
            @DisplayName("readStoryFromFile gives correct story back")
            void readFromFileTest() throws IOException, FileFormatException, InvalidFileException {
                assertEquals(FileHandling.readStoryFromFile("src/test/resources/fileHandlingTest.paths"), storyTest);
            }
        }

        @Nested
        @DisplayName("With Link´s")
        class fileHandlingOnlyOpeningPassageAndLinks {
            @BeforeEach
            void createStoryWithOnlyOpeningPassageLinks() {
                Link linkExit = new Link("You exit the building", "Escaped!");
                Link linkAnotherRoom = new Link("Try to open the door", "Another room");
                passageBeginnings.addLink(linkAnotherRoom);
                passageBeginnings.addLink(linkExit);
                storyTest = new Story("fileHandlingTest", passageBeginnings);
            }

            @Test
            @DisplayName("writeStoryToFile does not throw exception when it should not")
            void writeToFileTest() {
                assertDoesNotThrow(() -> FileHandling.writeStoryToFile(storyTest,"src/test/resources/"));
            }

            @Test
            @DisplayName("readStoryFromFile gives correct story back")
            void readFromFileTest() throws IOException, FileFormatException, InvalidFileException {
                assertEquals(FileHandling.readStoryFromFile("src/test/resources/fileHandlingTest.paths"), storyTest);
            }
        }

        @Nested
        @DisplayName("With Link´s and Action´s")
        class fileHandlingOnlyOpeningPassageAndLinksAndActions {
            @BeforeEach
            void createStoryWithOnlyOpeningPassageLinksActions() {
                Link linkAnotherRoom = new Link("Try to open the door", "Another room");
                HealthAction healthActionAnotherRoom = new HealthAction(50);
                GoldAction goldActionAnotherRoom = new GoldAction(30);
                linkAnotherRoom.addAction(healthActionAnotherRoom);
                linkAnotherRoom.addAction(goldActionAnotherRoom);
                Link linkExit = new Link("You exit the building", "Escaped!");
                InventoryAction inventoryActionExit = new InventoryAction("Sword");
                linkExit.addAction(inventoryActionExit);
                passageBeginnings.addLink(linkAnotherRoom);
                passageBeginnings.addLink(linkExit);
                storyTest = new Story("fileHandlingTest", passageBeginnings);
            }

            @Test
            @DisplayName("writeStoryToFile does not throw exception when it should not")
            void writeToFileTest() {
                assertDoesNotThrow(() -> FileHandling.writeStoryToFile(storyTest,"src/test/resources/"));
            }

            @Test
            @DisplayName("readStoryFromFile gives correct story back")
            void readFromFileTest() throws IOException, InvalidFileException, FileFormatException {
                assertEquals(FileHandling.readStoryFromFile("src/test/resources/fileHandlingTest.paths"), storyTest);
            }
        }
    }
    @Nested
    @DisplayName("FileHandling a Story with multiple Passage´s")
    class fileHandlingMultiplePassageNoLink {
        Passage passageBeginnings;
        Passage passageAnotherRoom;
        Passage passageAnUnknownSpell;
        @BeforeEach
        void createStoryWithMultiplePassageNoLink() {
            passageBeginnings = new Passage("Beginnings", "You are in a small, dimly lit room. There is a door in front of you.");
            storyTest = new Story("fileHandlingTest", passageBeginnings);
            passageAnotherRoom = new Passage("Another room", "The door opens to another room. You see a desk with a large, dusty book.");
            passageAnUnknownSpell = new Passage("An unknown spell", "You speak out the spell, and your pockets suddenly feels heavier");
        }

        @Nested
        @DisplayName("Multiple Passage´s with no Link´s")
        class fileHandlingOnlyPassagesNoLinks{
            @BeforeEach
            void createStoryWithMultiplePassageNoLink() {
                passageBeginnings = new Passage("Beginnings", "You are in a small, dimly lit room. There is a door in front of you.");
                storyTest = new Story("fileHandlingTest", passageBeginnings);
                passageAnotherRoom = new Passage("Another room", "The door opens to another room. You see a desk with a large, dusty book.");
                passageAnUnknownSpell = new Passage("An unknown spell", "You speak out the spell, and your pockets suddenly feels heavier");
                storyTest.addPassage(passageAnotherRoom);
                storyTest.addPassage(passageAnUnknownSpell);
            }
            @Test
            @DisplayName("writeStoryToFile does not throw exception when it should not")
            void writeToFileTest() {
                assertDoesNotThrow(() -> FileHandling.writeStoryToFile(storyTest,"src/test/resources/"));
            }

            @Test
            @DisplayName("readStoryFromFile gives correct story back")
            void readFromFileTest() throws IOException, FileFormatException, InvalidFileException {
                assertEquals(FileHandling.readStoryFromFile("src/test/resources/fileHandlingTest.paths"), storyTest);
            }
        }
        @Nested
        @DisplayName("Multiple Passage´s and Link´s")
        class fileHandlingMultiplePassageAndLink {
            @BeforeEach
            void createStoryWithMultiplePassageLink() {
                Link linkAnotherRoom = new Link("Try to open the door", "Another room");
                Link linkExit = new Link("You exit the building", "Escaped!");
                passageBeginnings.addLink(linkAnotherRoom);
                passageBeginnings.addLink(linkExit);
                Passage passageAnotherRoom = new Passage("Another room", "The door opens to another room. You see a desk with a large, dusty book.");
                Link linkTheBookOfSpells = new Link("Open the book", "The book of spells");
                passageAnotherRoom.addLink(linkTheBookOfSpells);
                Link linkCheckYourPocket = new Link("Check your pockets", "You find gold");
                passageAnUnknownSpell.addLink(linkCheckYourPocket);
                storyTest.addPassage(passageAnotherRoom);
                storyTest.addPassage(passageAnUnknownSpell);
            }

            @Test
            @DisplayName("writeStoryToFile does not throw exception when it should not")
            void writeToFileTest() {
                assertDoesNotThrow(() -> FileHandling.writeStoryToFile(storyTest,"src/test/resources/"));
            }

            @Test
            @DisplayName("readStoryFromFile gives correct story back")
            void readFromFileTest() throws IOException, InvalidFileException, FileFormatException {
                assertEquals(FileHandling.readStoryFromFile("src/test/resources/fileHandlingTest.paths"), storyTest);
            }
        }
        @Nested
        @DisplayName("Multiple Passage´s, Link´s and Action´s")
        class fileHandlingMultiplePassageLinkAction {
            @BeforeEach
            void createStoryWithMultiplePassageLink() {
                Link linkAnotherRoom = new Link("Try to open the door", "Another room");
                HealthAction healthActionAnotherRoom = new HealthAction(50);
                GoldAction goldActionAnotherRoom = new GoldAction(30);
                linkAnotherRoom.addAction(healthActionAnotherRoom);
                linkAnotherRoom.addAction(goldActionAnotherRoom);
                Link linkExit = new Link("You exit the building", "Escaped!");
                InventoryAction inventoryActionExit = new InventoryAction("Sword");
                linkExit.addAction(inventoryActionExit);
                passageBeginnings.addLink(linkAnotherRoom);
                passageBeginnings.addLink(linkExit);
                Link linkTheBookOfSpells = new Link("Open the book", "The book of spells");
                GoldAction goldActionLinkTheBookOfSpells = new GoldAction(50);
                linkTheBookOfSpells.addAction(goldActionLinkTheBookOfSpells);
                passageAnotherRoom.addLink(linkTheBookOfSpells);
                Link linkCheckYourPocket = new Link("Check your pockets", "You find gold");
                ScoreAction scoreActionLinkCheckYourPocket = new ScoreAction(-20);
                linkCheckYourPocket.addAction(scoreActionLinkCheckYourPocket);
                HealthAction healthActionLinkCheckYourPocket = new HealthAction(-25);
                linkCheckYourPocket.addAction(healthActionLinkCheckYourPocket);
                passageAnUnknownSpell.addLink(linkCheckYourPocket);
                storyTest.addPassage(passageAnotherRoom);
                storyTest.addPassage(passageAnUnknownSpell);
            }

            @Test
            @DisplayName("writeStoryToFile does not throw exception when it should not")
            void writeToFileTest() {
                assertDoesNotThrow(() -> FileHandling.writeStoryToFile(storyTest,"src/test/resources/"));
            }

            @Test
            @DisplayName("readStoryFromFile gives correct story back")
            void readFromFileTest() throws IOException, FileFormatException, InvalidFileException {
                assertEquals(FileHandling.readStoryFromFile("src/test/resources/fileHandlingTest.paths"), storyTest);
            }
        }
    }

    @Test
    @DisplayName("readStoryFromFile throws exception when file does not exist")
    void readFromFileThrowsExceptionWhenFileDoesNotExist(){
        assertThrows(FileNotFoundException.class, () -> FileHandling.readStoryFromFile("notAFile.paths"));
    }

    @Test
    @DisplayName("readStoryFromFile throws exception when .paths file has incorrect name")
    void readStoryFromFileThrowsExceptionWhenFileHasIncorrectFormat() {
        assertThrows(InvalidFileException.class, () -> FileHandling.readStoryFromFile("src/test/resources/santaClaus.png"));
    }
}