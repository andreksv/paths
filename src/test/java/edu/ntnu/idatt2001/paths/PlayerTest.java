package edu.ntnu.idatt2001.paths;

import edu.ntnu.idatt2001.paths.businessLogic.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
public class PlayerTest {

    @Nested
    @DisplayName("Test constructor exception handling")
    class testConstructorExceptionHandling {
        @Test
        @DisplayName("The Player constructor throws exception when the name is blank")
        void IsExceptionThrownWhenNameIsBlank() {
            assertThrows(IllegalArgumentException.class, () -> new Player.Builder()
                    .name("")
                    .build());
        }

        @Test
        @DisplayName("The Player constructor throws exception when the name is null")
        void IsExceptionThrownWhenNameIsNull() {
            assertThrows(NullPointerException.class, () -> new Player.Builder()
                    .name(null)
                    .build());
        }

        @Test
        @DisplayName("The Player constructor throws exceptions when gold value is negative.")
        void IsExceptionThrownWhenGoldIsNegative() {
            assertThrows(IllegalArgumentException.class, () -> new Player.Builder()
                    .gold(-20)
                    .build());
        }

        @Test
        @DisplayName("The Player constructor throws exceptions when score value is negative.")
        void IsExceptionThrownWhenScoreIsNegative() {
            assertThrows(IllegalArgumentException.class, () -> new Player.Builder()
                    .score(-20)
                    .build());
        }

        @Test
        @DisplayName("The Player constructor throws exceptions when health value is negative.")
        void IsExceptionThrownWhenHealthIsNegative() {
            assertThrows(IllegalArgumentException.class, () -> new Player.Builder()
                    .health(-20)
                    .build());
        }

        @Test
        @DisplayName("The Player constructor throws exception when inventory value is null.")
        void IsExceptionThrownWhenInventoryIsNull() {
            assertThrows(NullPointerException.class, () -> new Player.Builder()
                    .inventory(null)
                    .build());
        }


        @Test
        @DisplayName("The Player constructor does not throw exceptions when it should not.")
        void ExceptionIsNotExpectedWhenGoldHealthAndScoreIsPositive() {
            List<String> inventory = new ArrayList<>();
            inventory.add("Sword");
            assertDoesNotThrow(() -> new Player.Builder()
                    .name("Bob")
                    .gold(10)
                    .score(23)
                    .health(23)
                    .inventory(inventory)
                    .build());
        }
    }

    @Nested
    @DisplayName("Test getName method")
    class testGetName {
        @Test
        @DisplayName("Test that getName gives correct name")
        void CheckGetNameGivesCorrectName() {
            String checkName = "Lele";
            Player player = new Player.Builder()
                    .name("Lele")
                    .build();
            assertEquals(checkName, player.getName());
        }

        @Test
        @DisplayName("Test that getName does not give wrong name")
        void CheckGetNameGivesWrongName() {
            String wrongName = "Bobby";
            Player bobby = new Player.Builder()
                    .name("Richard")
                    .build();
            assertNotEquals(wrongName, bobby.getName());
        }
    }

    @Nested
    @DisplayName("Test addHealth method")
    class addHealthTest {
        @Test
        @DisplayName("addHealth is able to increase the health of the player")
        void addHealthIsAbleToIncreaseTheHealthOfThePlayer(){
            Player jimmy = new Player.Builder()
                    .health(50)
                    .build();
            jimmy.addHealth(25);
            assertEquals(75, jimmy.getHealth());
        }

        @Test
        @DisplayName("addHealth is not able to decrease the health of the player below zero")
        void addHealthIsNotAbleToDecreaseTheHealthOfThePlayerBelowZero(){
            Player jimmy = new Player.Builder()
                    .health(50)
                    .build();
            jimmy.addHealth(-75);
            assertEquals(0, jimmy.getHealth());
        }
    }

    @Nested
    @DisplayName("Test addScore method")
    class addScoreTest {
        @Test
        @DisplayName("addScore is able to increase the score of the player")
        void NotExceptionWhenAddScoreIsPositive() {
            Player brian = new Player.Builder()
                    .score(20)
                    .build();
            int scoreAmount = 7;
            brian.addScore(scoreAmount);
            assertEquals(27, brian.getScore());
        }

        @Test
        @DisplayName("addScore is not able to decrease the score of the player below zero")
        void addScoreIsNotAbleToDecreaseTheScoreOfThePlayerBelowZero(){
            Player jimmy = new Player.Builder()
                    .score(50)
                    .build();
            jimmy.addScore(-75);
            assertEquals(0, jimmy.getScore());
        }
    }

    @Nested
    @DisplayName("Test addGold method")
    class addGoldTest{
        @Test
        @DisplayName("addGold is able to increase the gold of the player")
        void NotExceptionWhenAddScoreIsPositive() {
            Player brian = new Player.Builder()
                    .gold(100)
                    .build();
            int goldAmount = 12;
            brian.addGold(goldAmount);
            assertEquals(112, brian.getGold());
        }

        @Test
        @DisplayName("addGold is not able to decrease the gold of the player below zero")
        void addGoldIsNotAbleToDecreaseTheGoldOfThePlayerBelowZero(){
            Player jimmy = new Player.Builder()
                    .gold(100)
                    .build();
            jimmy.addHealth(-200);
            assertEquals(0, jimmy.getScore());
        }
    }

    @Nested
    @DisplayName("Test addToInventory method")
    class inventoryMethodsTest {
        @Test
        @DisplayName("addToInventory throw exception when trying to add null")
        void exceptionWhenAddInventoryIsNull() {
            Player lola = new Player.Builder()
                    .build();
            assertThrows(NullPointerException.class, () -> lola.addToInventory(null));
        }

        @Test
        @DisplayName("addToInventory throw exception when trying to add an empty String")
        void exceptionWhenStringIsEmpty() {
            Player jax = new Player.Builder()
                    .build();
            assertThrows(IllegalArgumentException.class, () -> jax.addToInventory(""));


        }

        @Test
        @DisplayName("addToInventory does not throw exception when it should not")
        void noExceptionWhenThereIsAItem() {
            Player ryan = new Player.Builder().build();
            String stick = "Stick";
            assertDoesNotThrow(() -> ryan.addToInventory(stick));
        }

        @Test
        @DisplayName("getInventory gives correct object back")
        void CanGetInventory() {
            Player jenny = new Player.Builder()
                    .build();
            String item = "Cat";
            jenny.addToInventory(item);
            assertTrue(jenny.getInventory().contains(item));
        }
    }
}
