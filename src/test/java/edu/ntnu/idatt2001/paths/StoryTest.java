package edu.ntnu.idatt2001.paths;

import edu.ntnu.idatt2001.paths.businessLogic.Link;
import edu.ntnu.idatt2001.paths.businessLogic.Passage;
import edu.ntnu.idatt2001.paths.businessLogic.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class StoryTest {
    @Nested
    @DisplayName("Testing getTitle and getOpeningPassage")
    class getMethods {
        Story storyTest;
        @BeforeEach
        void createStoryObject() {
            Passage openingPassageTest = new Passage("A happy morning", "Once upon a time there was a...");
            storyTest = new Story("The bountiful bouquet", openingPassageTest);
        }

        @Test
        @DisplayName("getTitle gives correct value")
        void getTitleTest() {
            assertEquals("The bountiful bouquet", storyTest.getTitle());
        }

        @Test
        @DisplayName("getOpeningPassage gives correct value")
        void getOpeningPassageTest() {
            assertEquals(storyTest.getOpeningPassage(), new Passage("A happy morning", "Once upon a time there was a..."));
        }
    }
    @Nested
    @DisplayName("Testing addPassage and getPassage method")
    class addPassageGetPassageMethods{
        Story storyTest;
        @BeforeEach
        void createStoryObject() {
            Passage openingPassageTest = new Passage("A happy morning", "Once upon a time there was a...");
            storyTest = new Story("The bountiful bouqeut", openingPassageTest);
        }
        @Test
        @DisplayName("addPassage does not throw exception when it should not")
        void addNewPassageTest(){
            Passage passageTest1 = new Passage("A fight", "You found yourself on a battlefield with enemies all around you when suddenly...");
            Passage passageTest2 = new Passage("Run away", "Run for your life before they get to you!");
            assertDoesNotThrow(() -> storyTest.addPassage(passageTest1));
            assertDoesNotThrow(() -> storyTest.addPassage(passageTest2));
        }

        @Test
        @DisplayName("addPassage throws exception when it should")
        void addSamePassageTest() {
            Passage passageTest = new Passage("A fight", "You found yourself on a battlefield with enemies all around you when suddenly...");
            storyTest.addPassage(passageTest);
            assertThrows(IllegalArgumentException.class, () -> {storyTest.addPassage(passageTest);fail("Method did not throw IllegalArgumentException as expected");});
        }

        @Test
        @DisplayName("getPassage gives correct Passage")
        void getPassageTest(){
            Passage passageTest = new Passage("A fight", "You found yourself on a battlefield with enemies all around you when suddenly...");
            storyTest.addPassage(passageTest);
            Link linkPassage = new Link("Different title",passageTest.getTitle());
            assertEquals(storyTest.getPassage(linkPassage),passageTest);
        }
        /* Should this be removed? */
        /**
        @Test
        @DisplayName("getPassage throws exception when it should")
        void getPassageWrongLinkTest(){
            Passage passageTest = new Passage("A fight", "You found yourself on a battlefield with enemies all around you when suddenly...");
            storyTest.addPassage(passageTest);
            Link linkPassage = new Link("Running away", "Why are you running?");
            assertThrows(NullPointerException.class, () -> {storyTest.getPassage(linkPassage);fail("Method did not throw NullPointerException as expected");});
        }**/

        @Test
        @DisplayName("getPassages gives correct Passages back")
        void getPassagesTest(){
            Passage passageTest1 = new Passage("A fight", "You found yourself on a battlefield with enemies all around you when suddenly...");
            Passage passageTest2 = new Passage("A mosnter", "There is a giant troll in front of you!");
            storyTest.addPassage(passageTest1);
            storyTest.addPassage(passageTest2);
            assertTrue(storyTest.getPassages().contains(new Passage("A mosnter", "There is a giant troll in front of you!")) && storyTest.getPassages().contains(new Passage("A fight", "You found yourself on a battlefield with enemies all around you when suddenly...")));
        }
    }

    @Nested
    @DisplayName("Test getLinks and getBrokenLinks method")
    class testGetLinksAndGetBrokenLinks{
        Story storyTest;
        @BeforeEach
        void createStoryObject() {
            Passage passageBeginnings = new Passage("Beginnings", "You are in a small, dimly lit room. There is a door in front of you.");
            Link linkAnotherRoom = new Link("Try to open the door", "Another room");
            Link linkExit = new Link("You exit the building", "Escaped!");
            passageBeginnings.addLink(linkAnotherRoom);
            passageBeginnings.addLink(linkExit);
            storyTest = new Story("fileHandlingTest", passageBeginnings);
            Passage passageAnUnknownSpell = new Passage("An unknown spell", "You speak out the spell, and your pockets suddenly feels heavier");
            Passage passageAnotherRoom = new Passage("Another room", "The door opens to another room. You see a desk with a large, dusty book.");
            Link linkTheBookOfSpells = new Link("Open the book", "The book of spells");
            passageAnotherRoom.addLink(linkTheBookOfSpells);
            Link linkCheckYourPocket = new Link("Check your pockets", "You find gold");
            Link linkScreamLoud = new Link("You scream", "Ahhhhhhhh");
            passageAnUnknownSpell.addLink(linkScreamLoud);
            passageAnUnknownSpell.addLink(linkCheckYourPocket);
            storyTest.addPassage(passageAnotherRoom);
            storyTest.addPassage(passageAnUnknownSpell);
        }

        @Test
        @DisplayName("getLinks gives correct Links back")
        void getLinksGivesCorrectLinks(){
            ArrayList<Link> allLinks = new ArrayList<>();
            allLinks.add(new Link("Check your pockets", "You find gold"));
            allLinks.add(new Link("Open the book", "The book of spells"));
            allLinks.add(new Link("You exit the building", "Escaped!"));
            allLinks.add(new Link("Try to open the door", "Another room"));
            allLinks.add(new Link("You scream", "Ahhhhhhhh"));

            ArrayList<Link> getLinks = (ArrayList<Link>) storyTest.getLinks();
            allLinks.forEach(link -> assertTrue(getLinks.contains(link)));
        }

        @Test
        @DisplayName("getBrokenLinks gives correct Links back")
        void getBrokenLinksGiveCorrectLinks(){
            ArrayList<Link> brokenLinks = new ArrayList<>();
            brokenLinks.add(new Link("You scream", "Ahhhhhhhh"));
            brokenLinks.add(new Link("Check your pockets", "You find gold"));
            brokenLinks.add(new Link("You exit the building", "Escaped!"));

            ArrayList<Link> getBrokenLinks = (ArrayList<Link>) storyTest.getBrokenLinks();
            brokenLinks.forEach(link -> assertTrue(getBrokenLinks.contains(link)));
        }
    }

    @Nested
    @DisplayName("Test getBrokenPassages and removePassage methods")
    class testGetBrokenPassagesAndRemovePassage{
        Story storyTest;

        @BeforeEach
        void createStoryObject(){
            Passage passageBeginnings = new Passage("Beginnings", "You are in a small, dimly lit room. There is a door in front of you.");
            Link linkAnotherRoom = new Link("Try to open the door", "Another room");
            passageBeginnings.addLink(linkAnotherRoom);
            storyTest = new Story("fileHandlingTest", passageBeginnings);
            Passage passageAnUnknownSpell = new Passage("The book of spells", "You speak out the spell, and your pockets suddenly feels heavier");
            Passage passageAnotherRoom = new Passage("Another room", "The door opens to another room. You see a desk with a large, dusty book.");
            Link linkGoBack = new Link("Go back", "Beginnings");
            Link linkTheBookOfSpells = new Link("Open the book", "The book of spells");
            passageAnotherRoom.addLink(linkTheBookOfSpells);
            passageAnotherRoom.addLink(linkGoBack);
            storyTest.addPassage(passageAnotherRoom);
            storyTest.addPassage(passageAnUnknownSpell);

            Passage passageNotRefered1 = new Passage("Not refered to 1", "content");
            Passage passageNotRefered2 = new Passage("Not refered to 2", "content");
            Passage passageNotRefered3 = new Passage("Not refered to 3", "content");
            storyTest.addPassage(passageNotRefered1);
            storyTest.addPassage(passageNotRefered2);
            storyTest.addPassage(passageNotRefered3);
        }

        @Test
        @DisplayName("getBrokenPassages gives correct Passages back")
        void getBrokenPassagesGivesCorrectPassagesBack(){
            ArrayList<Passage> brokenPassages = new ArrayList<>();
            brokenPassages.add(new Passage("Not refered to 1", "content"));
            brokenPassages.add(new Passage("Not refered to 2", "content"));
            brokenPassages.add(new Passage("Not refered to 3", "content"));
            brokenPassages.forEach(passage -> assertTrue(storyTest.getBrokenPassages().contains(passage)));
        }

        @Test
        @DisplayName("removePassage does not throw exception when it should not")
        void removePassageDoesNotThrowExceptionIfThereAreOtherPassagesLinkingIt(){
            assertDoesNotThrow(()->storyTest.removePassage(new Link("Not refered to 1","Not refered to 1")));
        }

        @Test
        @DisplayName("removePassage does throw exception when it should")
        void removePassageDoesThrowExceptionWhenItShould(){
            assertThrows(IllegalArgumentException.class, () -> storyTest.removePassage(new Link("Try to open the door", "Another room")));
        }
    }
}
